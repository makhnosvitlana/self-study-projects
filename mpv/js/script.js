$('.img-small').on('click', function () {
    $(this).siblings('.gallery-overlay').css('display', 'block');
});

$('.close').on('click', function () {
    $('.gallery-overlay').css('display', 'none');
});

$('.prev'). on('click', function () {
    console.log('prev');
    $(this).parents('.gallery-overlay').css('display', 'none');
    $(this).parents('.gallery__item').prev().children('.gallery-overlay').css('display', 'block');
});


$('.next'). on('click', function () {
    console.log('next');
    $(this).parents('.gallery-overlay').css('display', 'none');
    $(this).parents('.gallery__item').next().children('.gallery-overlay').css('display', 'block');
});