var ProfilesCollection = Backbone.Collection.extend({
    initialize: function() {
        this.set(JSON.parse(window.localStorage.getItem('profilesCollection')) || []);
        this.on('update', this.updateStorage);
    },

    updateStorage: function () {
        window.localStorage.setItem('profilesCollection', JSON.stringify(this.models));
    },

    getVipProfiles: function () {
        return _.filter(this.toJSON(), function (profile) {
            return profile.price > 500
        });
    }
});

var profilesCollection = new ProfilesCollection();

var ListView = Backbone.View.extend({

    tmplFn: doT.template($('#profiles-template').html()),

    initialize: function() {
        this.render();
        this.listenTo(this.collection, 'update', this.render);
    },

    events: {
        'click .btn--add-profile': 'renderAddForm',
        'click .profiles-list__item': 'renderEditForm',
        'change  .vip': 'handleVipChange'
    },

    render: function() {
        if ($('.vip').is(':checked')) {
            this.$el.html(this.tmplFn(this.collection.getVipProfiles()));
            document.querySelector('.vip').checked = true;
        } else {
            this.$el.html(this.tmplFn(this.collection.toJSON()));
        }
        $('.profiles-container').append(this.$el);
    },

    renderAddForm: function() {
        addFormView.render();
    },

    renderEditForm: function(e) {
        var selectedProfile = this.collection.get($(e.target).data('id')).toJSON();
        editFormView.render(selectedProfile);
    },

    handleVipChange: function () {
        this.render();
    }
});

var listView = new ListView({
    collection: profilesCollection
});

var AddFormView = Backbone.View.extend({

    tmplFn: doT.template($('#add-form-template').html()),

    render: function() {
        this.$el.html(this.tmplFn());
        $('.form-container').append(this.$el);
    },

    events: {
        'click .btn--save': 'handleClickOnSave',
    },

    handleClickOnSave: function() {
        profilesCollection.add(this.getFormData());
        this.hide();
    },

    getUniqId: function() {
        return '_' + Math.random().toString(36).substr(2, 9);
    },

    getFormData: function() {
        var addFormInputs = $('.add-profile-form input');
        var profile = {};
        addFormInputs.each(function (index, input) {
            profile[input.name] = input.value;
        });
        profile.id = this.getUniqId();
        return profile;
    },

    hide: function() {
        $('.add-profile-form').addClass('hidden');
    }
});

var addFormView = new AddFormView();

var EditFormView = Backbone.View.extend({

    tmplFn: doT.template($('#edit-form-template').html()),

    render: function(profile) {
        this.$el.html(this.tmplFn(profile));
        $('.form-container').append(this.$el);
    },

    events: {
        'click .btn--update': 'onUpdate',
        'click .btn--delete': 'onDelete'
    },

    onUpdate: function() {
        profilesCollection.add(this.getFormData(), {
            merge: true
        });
        this.hide();
    },

    onDelete: function() {
        profilesCollection.remove(this.getFormData().id);
        this.hide();

    },

    getFormData: function() {
        var editFormInputs = $('.edit-profile-form input');
        var updatedProfile = {};
        editFormInputs.each(function (index, input) {
            updatedProfile[input.name] = input.value;
        });
        updatedProfile.id = $('.edit-profile-form').data('id');
        return updatedProfile;
    },

    hide: function() {
        $('.edit-profile-form').addClass('hidden');
    }

});

var editFormView = new EditFormView();