let game = {
    isOver: false,
    el: document.querySelector('.game'),
    init: function() {
        dino.init();
        let id = setInterval(function() {
            if (!game.isOver) {
                let obstacle = new Obstacle();
                obstacle.init();
            }
        }, 1500);
    }
};

let score = {
    el: document.querySelector('.score'),
    count: 0,
    draw: function() {
        this.el.innerHTML = this.count;
    }
};

let Obstacle = function() {};

Obstacle.prototype = {
    x: 1000,
    y: 300,
    width: 22,
    height: 46,
    el: null,
    type: [],
    state: 0,
    init: function() {
        this.el = document.createElement('div');
        this.el.classList.add('obstacle');
        game.el.appendChild(this.el);
        this.draw();
        this.move();
    },
    draw: function() {
        this.el.style.width = this.width + 'px';
        this.el.style.height = this.height + 'px';
        this.el.style.top = this.y + 'px';
        this.el.style.left = this.x + 'px';
    },
    move: function() {
        let step = 8;
        let id = setInterval(function() {
            if (this.x > -(this.width)) {
                this.x -= step;
                this.draw();
                if (dino.isDead(this, dino)) {
                    game.isOver = true;
                    game.el.classList.add('gameover');
                    clearInterval(id);
                }
            } else {
                clearInterval(id);
                score.count++;
                score.draw();
                this.remove();
            }
        }.bind(this), 25)
    },
    remove: function() {
        this.el.parentNode.removeChild(this.el);
    }
};

let dino = {
    el: document.querySelector('.dino'),
    x: 500,
    y: 300,
    width: 44,
    height: 45,
    positions: ['-88px', '-132px'],
    state: 0,
    isJumping: false,
    init: function() {
        this.draw();
        this.run();
        this.subscribe();
    },
    draw: function() {
        this.el.style.width = this.width + 'px';
        this.el.style.height = this.height + 'px';
        this.el.style.top = this.y + 'px';
        this.el.style.left = this.x + 'px';
    },
    run: function() {
        let id = setInterval(function() {
            if (this.isJumping) {
                return;
            }

            if (game.isOver) {
                clearInterval(id);
            }

            this.changePosition();
        }.bind(this), 250)
    },
    changePosition: function() {
        this.state++;

        if (this.state === this.positions.length) {
            this.state = 0;
        }
        
        this.el.style.backgroundPositionX = this.positions[this.state];
    },
    subscribe: function() {
        document.addEventListener('keydown', function(event) {
            if (event.which === 32) {
                this.jump();
            }
        }.bind(this))
    },
    jump: function() {

        if (this.isJumping || game.isOver) {
            return;
        }

        let step = 10;
        let counter = 21;
        this.isJumping = true;
        let id = setInterval(function() {
            if (game.isOver) {
                clearInterval(id);
                return;
            }

            if (counter > 0) {
                this.y -= step;
                this.draw();
                step--;
            } else {
                this.isJumping = false;
                clearInterval(id);
            }
            counter--;
        }.bind(this), 25)
    },
    isDead: function(cactus, hero) {
        if(
            (((hero.y > cactus.y) && (hero.y < cactus.y + cactus.height)) || ((hero.y + hero.height > cactus.y) && (hero.y + hero.height < cactus.y + cactus.height))) &&
            (((hero.x > cactus.x) && (hero.x < cactus.x + cactus.width )) || ((hero.x + hero.width  > cactus.x) && (hero.x + hero.width  < cactus.x + cactus.width )))
        ) {
            return true;
        } else {
            return false;
        }}
};

game.init();
