chai.should();

describe('Functions that are expected to return a boolean', function () {
    describe('isUndefined', function () {
        it('returns false if given value is not undefined', function () {
            isUndefined('undefined').should.equal(false);
        });

        it('returns a boolean', function () {
            chai.expect(isUndefined(undefined)).to.be.a('boolean');
        });

        it('returns true if a given value is undefined', function () {
            chai.assert(isUndefined(undefined), true);
        });
    });

    describe('isEven', function () {
        it('returns false if a given value is an odd number', function () {
            isUndefined(isEven(1)).should.equal(false);
        });

        it('returns a boolean', function () {
            chai.expect(isEven(2)).to.be.a('boolean');
        });

        it('returns false if a given value is a string', function () {
            chai.assert(isEven('2'), false);
        });
    })
});

describe('Functions that are expected to return an array', function () {
    describe('getPositiveNumbers', function () {
        it('returns an empty array if no positive numbers', function () {
            getPositiveNumbers([-1, -2, -3, -4]).should.be.an('array').with.a.lengthOf(0);
        });

        it('returns an array without negative numbers', function () {
            chai.expect(getPositiveNumbers(-1, -2, 3, 4)).to.not.include(-1);
        });

        it('returns an array with only positive numbers', function () {
            chai.assert(getPositiveNumbers(-1, -2, -3, -4, 5, 5), [5, 5])
        })
    });

    describe('unshift', function () {
        it ('returns an array that includes the second argument', function () {
            unshift([1, 2, 3], 0).should.include.members([0]);
        });

        it('returns array with a length that is greater than the length of a given array', function () {
            chai.expect(unshift([1, 2, 3], 0)).to.an('array').with.a.lengthOf([1, 2, 3].length + 1);
        });

        it('return an array where the first element is a second argument', function () {
            chai.assert(unshift([1, 2, 3], 0), [0, 1, 2, 3]);
        })
    });

    describe('compact', function () {
        it('returns the same old array if it did not include undefined values', function () {
            compact([1, 2, 'three', null, false]).should.be.an('array').with.a.lengthOf([1, 2, 'three', null, false].length);
        });

        it('returns an array without undefined elements', function () {
            chai.expect(compact([1, undefined, 3, 4])).to.not.include([undefined]);
        });

        it('returns an empty array if the given array consisted of only undefined values', function () {
            chai.assert(compact([undefined, undefined, undefined]), []);
        })
    })
});

describe('Functions that are expected to return a primitive value', function () {
    describe('join', function () {
        it('returns an empty string if a given array is empty', function () {
            join([]).should.be.a('string').with.a.lengthOf(0);
        });

        it('returns a string that contains stringified array elements', function () {
            chai.expect(join([1, 'two', null, undefined, true], '/')).to.have.string('null');
        });

        it('returns a string', function () {
            chai.assert.typeOf(join([1, 'two', null, undefined, true], '/'), 'string');
        })
    });

    describe('indexOf', function () {
        it('returns a number', function () {
            indexOf([1, 2, 3], 3).should.be.a('number');
        });

        it('returns a positive number if the search value is present in an array', function () {
            chai.expect(indexOf([-4, -3, -2, -1], -4)).to.satisfy(function(num) { return num => 0; });
        });

        it('returns an index of the search value in an array', function () {
            chai.assert.equal(indexOf([1, 2, 3], 3), 2);
        })
    });

    describe('abs', function () {
        it('returns only positive numbers', function () {
            abs(-1).should.be.above(0);
        });

        it('returns only positive numbers', function () {
            chai.expect(abs(-1000)).to.be.at.least(0);
        });

        it('returns an absolute value of a number', function () {
            chai.assert.equal(abs(20), 20)
        })
    })
});

describe('Functions that work with objects', function () {
    describe('extend', function () {
        it('should return an object with properties of both objects', function() {
            var result = extend({name: 'moe', surname: 'doe'}, {name: 'john', age: 25});

            chai.expect(result).to.have.all.keys('name', 'surname', 'age');
        });

        it('values of the second object must override the values of the first object', function() {
            var a = { name: 'moe' };
            var b = { name: 'john' };
            var result = extend(a, b);

            chai.expect(result.name).to.equal(b.name);
        });

        it('should not write properties that object b inherited from its prototype', function() {
            var testProto = function(name) {
                this.name = name;
            };
            testProto.prototype.surname = 'doe';
            var b = new testProto('john');
            var a = { age: 22};
            var result = extend(a, b);

            chai.expect(result).to.not.have.property('surname');
        })
    });

    describe('extend2 - extends the object given as the first argument with the properties of the rest arguments', function() {
        it('should return an object with properties of all objects', function() {
            var a = { name: 'john' };
            var b = { surname: 'doe' };
            var c = { age: 30 };
            var result2 = extend2(a, b, c);

            chai.expect(result2).to.have.all.keys('name', 'surname', 'age');
        });

        it('the properties of the objects with higher index must override the properties of the objects with lower index', function() {
            var a = { surname: 'john' };
            var b = { surname: 'doe' };
            var c = { surname: 'smith' };
            var result2 = extend2(a, b, c);

            chai.expect(result2.surname).to.equal(c.surname);
        });

        it('should not write properties that objects inherited from their prototypes', function() {
            var testProto = function(name) {
                this.name = name;
            };
            testProto.prototype.surname = 'doe';

            var b = new testProto('john');
            var c = new testProto('jim');
            var a = {age: 33};
            var result2 = extend2(a, b, c);

            chai.expect(result2).to.not.have.property('surname');
        })
    });

    describe('defaults', function () {
        it('returns an object', function () {
            defaults({flavor: "chocolate"}, {flavor: "vanilla", sprinkles: "lots"}).should.be.an('object');
        });

        it('returns an object where initial properties are not redefined', function () {
            chai.expect(defaults({flavor: "chocolate"}, {flavor: "vanilla", sprinkles: "lots"})).to.include({flavor: "chocolate"});
        });

        it('returns an object with properties from initial and a given object', function () {
            chai.assert(defaults({flavor: "chocolate"}, {flavor: "vanilla", sprinkles: "lots"}), {flavor: "chocolate", sprinkles: "lots"})
        })
    })
});

