//При клике на кнопку изменить цвет текста

$('.b1').on('click', function () {
   $('.t1').css('color', 'green');
});

//При клике на кнопку изменить цвет фона

$('.b2').on('click', function () {
   $('.t2').css('background', 'green');
});

//При клике на кнопку изменить путь ссылки

$('.b3').on('click', function () {
    $('.t3 a').attr('href', 'https://api.jquery.com/');
});

//При клике на кнопку изменить путь к картинке

$('.b4').on('click', function () {
   $('.t4 img').attr('src', 'images/kat.png');
});

//При клике на кнопку изменить id элемента

$('.b5').on('click', function () {
   $('.t5').attr('id', 'test5');
});

//При клике на кнопку изменить текст

$('.b6').on('click', function () {
   $('.t6').text('New Text');
});

//При клике на кнопку изменить HTML

$('.b7').on('click', function () {
   $('.t7').html('<div>Changed <p>HTML</p></div>')
});

//При клике на кнопку изменить размер шрифта

$('.b8').on('click', function () {
   $('.t8').css('font-size', '24px');
});

//При клике на кнопку добавить класс 'hi'

$('.b9').on('click', function () {
  $('.t9').addClass('hi');
});

//При клике на кнопку удалить класс 'hi'

$('.b10').on('click', function () {
    $('.t10').removeClass('hi');
});

//При клике на кнопку добавлять/удалять (в зависимости от наличия) класс 'hi'

$('.b11').on('click', function () {
    $('.t11').toggleClass('hi');
});

//При клике на любом эелементе страницы выводить className элемента

$('body').on('click', function (e) {
    $('.t12').text(e.target.classList);
});

//При клике на кнопку перевести текст на английский язык

$('.b13').on('click', function () {
    var content = $('.t13');
    content.text(content.attr('data-en'));
});

//Вывести на экран анкету Жасмин

$('.b14').on('click', function () {
    var newWhore = $('.whore').clone().appendTo('.whores-container');
    var can_come = whore.can_come ? '+' : '-';
    newWhore.children('.whore-name').text(whore.name);
    newWhore.children('img').attr('src', whore.photo);
    newWhore.children('.whore-age').text('Возраст:' + ' ' + whore.age);
    newWhore.children('.whore-boobs').text('Размер груди: ' + whore.boobs);
    newWhore.children('.whore-height').text('Рост:' + ' ' + whore.height);
    newWhore.children('.whore-weight').text('Вес:' + ' ' + whore.weight);
    newWhore.children('.whore-phone').text(whore.phone);
    newWhore.children('.whore-can-come').text(can_come);
    newWhore.children('.whore-teaser').text(whore.teaser);
});

//Переместить рыбу из первого контейнера во второй (и из второго в первый)

var fish2;
$('.b15').on('click', function () {
    if (fish2 === undefined) {
        fish2 = $('.fish').clone().appendTo($('.cat-container-2'));
        $('.cat-container-1').children('.fish').toggle(1800);
    } else {
        fish2.toggle(1800);
        $('.cat-container-1').children('.fish').toggle(1800);
    }
});

//Удалить зуб

$('.b16').on('click', function () {
    $('.tooth').remove();
});

//Хочу чтоб лыжник бесконечно ехал вправо (сдвиг на 5px каждые 16ms). При нажатии на кнопку "Стоп!" останавливался.

$('.b17').on('click', function () {
    var currentPosition = 0;
    var go = setInterval(function () {
        currentPosition += 5;
        $('.skier').css('left', currentPosition + 'px');
    }, 16);

    $('.b17-2').on('click', function () {
        clearInterval(go);
    })
});

//Mikki mouse

var createMikki = function() {
    var i;
    mikki.forEach(function (element) {
        for (i = 0; i < element.length; i++) {
            if (element[i] === 'X') {
                $('<div></div>').addClass('mikki_tile').css('background', 'black').appendTo($('.mikki_tiles'));
            } else {
                $('<div></div>').addClass('mikki_tile').appendTo($('.mikki_tiles'));
            }
        }
    })
};


createMikki();

//Создать мир Марио

$('.b18').on('click', function () {
    var i;
    var j;
    var newTile;
    var newTileClass = 'x_';
    var left;
    var top = -16;
    for (i = 0; i < map.length; i++) {
        top += 16;
        left = 0;
        for (j = 0; j < map[i].length; j++) {
            newTile = $('<div>');
            newTile.addClass('tile');
            newTile.css('left', left + 'px');
            newTile.css('top', top + 'px');
            left += 16;

            if (map[i][j] !== ' ') {
                newTileClass += map[i][j];
                newTile.addClass(newTileClass);
                newTileClass = 'x_';
            }
            $('.scene').append(newTile);
        }
    }
});

//Кликая по кнопкам "предыдущая" и "следующая" я хочу чтобы менялись соответственно слайды

$('.next').on('click', function () {
    var last = $('.slider').children().last();
    var current = $('.slider .active');
    if (!(last.hasClass('active'))) {
        current.removeClass('active');
        current.next().addClass('active')
    }
});

$('.previous').on('click', function () {
   var first = $('.slider').children().first();
   var current = $('.slider .active');
   if(!(first.hasClass('active'))) {
       current.removeClass('active');
       current.prev().addClass('active')
   }
});

//Кликая вопросам я хочу видеть ответы на них

$('dt').each(function (element) {
    $(this).on('click', function () {
        $(this).toggleClass('active');
    })
});

//Кликая по вкладкам я хочу видеть связанное содержимое

$('.x-tab').on('click', function () {
    $('.x-tab.active').removeClass('active');
    $(this).addClass('active');
    $('.x-item').each(function () {
        $(this).removeClass('active');
        if ($(this).attr('data-section') === $('.x-tab.active').attr('data-tab')) {
            $(this).addClass('active');
       }
    });
});

//Кликая на кнопку Login я хочу чтобы затенялся экран и по середине экрана появлялось окошко для авторизации закрыть которое можно кликнув по кнопке с крестиком

$('.show-login-pop-up').on('click', function () {
    $('.overlay').removeClass('hidden');
    $('.pop-up.pop-up--login').removeClass('hidden');
    $('.close').on('click', function () {
        $('.overlay').addClass('hidden');
        $('.pop-up--login').addClass('hidden');
    })
});

//Отменить действие по-умолчанию при клике на ссылку

$('.link-ebanoe').on('click', function (e) {
   e.preventDefault();
});

//Кликнув на кнопку "Load more" необходимо отправить HTTP GET запрос по адресу 'http://127.0.0.1:3000/'
// и получив ответ в виде массива игр добавить их в контейнер

 $('.load-more').on('click', function () {
     var gamesResponse;
     var games = {};
     var tempFn;
     $.get('http://127.0.0.1:3000/', function (data, status) {
         if(status === 'success') {
             gamesResponse = JSON.parse(data);
             games.array = gamesResponse;
             tempFn = doT.template('{{~it.array :value:index}}<div class="game"><img src={{=value.imageURL}}><h6>{{=value.name}}</h6></div>{{~}}');
             $('.games').append(tempFn(games));
         }
         if (status !== 'success') {
             console.log(status);
         }
     })
 });