var formElements = $('.add-profile-form').find('input');
var oldWhores = [];
var existingWhores = window.localStorage.getItem('whores');
var currentProfile;
var editFormInputs;
var formData;
var formDataProperty;
var updatedWhores = [];
var updatedElement;
var count;

//Get existing whores list from local storage into an array

var getOldWhores = function () {
    var whoresArray;
    if (existingWhores !== null && existingWhores.length !== 0) {
        whoresArray = existingWhores.split('next');
        whoresArray.pop();
        console.log(whoresArray);
        whoresArray.forEach(function (element) {
            oldWhores.push(JSON.parse(element));
        });
    } else {
        $('.nowhores').css('display', 'block');
    }
    console.log(oldWhores);
};

getOldWhores();

//Append data from the whores array to html

var showProfiles =  function() {
    var profilesContainer;
    var templateFunction;
    var profiles = {};
    profiles.array = oldWhores;
    profilesContainer = document.querySelector('.profiles-container');
    templateFunction = doT.template('{{~it.array :value:index}}<li class="profile" id="{{= value.id}}"><span>Nick:</span> {{=value.nickname}}<br><span>Age:</span> {{=value.age}}<br><span>Price:</span> {{=value.price}}</li>{{~}}');
    profilesContainer.innerHTML += templateFunction(profiles);
};

showProfiles();

//Add-profile button opens a form

$('.add_whore').on('click', function () {
    $('.add-profile-form').css('display', 'block');
    $(this).css('display', 'none');
});

//Generate a unique id for each profile

var generateID = function () {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
};

//Form validation

var validateForm = function(element) {
    var namePattern = /^[a-zA-Z]{2,15}$/;
    var agePattern = /^[0-9]{2}$/;
    var pricePattern = /^[0-9]{1,6}$/;
    var error;
    var value = element.val();
    var validate = function(pattern, value){
        if (!value.match(pattern)) {
            console.log('false');
            element.addClass('invalid');
            if (element.siblings('.error').length === 0) { //add error element only once
                $('<p class="error"></p>').text(error).insertAfter(element);
            }
            return false;
        } else {
            return true;
        }
    };

    console.log(value);
    console.log(element.attr('name'));

    if (element.attr('name') === 'firstName' || element.attr('name') === 'lastName' || element.attr('name') === 'nickname') {
        error = 'The field must contain 2 to 15 letters';
        return validate(namePattern, value);
    }
    if (element.attr('name') === 'age') {
        error = 'The field must contain 2 digits';
        return validate(agePattern, value);
    }
    if (element.attr('name') === 'price') {
        error = 'The field must contain only digits';
        return validate(pricePattern, value);
    }
};

//Form validation when input loses focus

formElements.each(function () {
   $(this).on('change', function () {
       validateForm($(this));
   });

    $(this).on('focus', function () {
        $(this).removeClass('invalid');
        $(this).siblings('.error').remove();
    })
});

//When the save btn is clicked, the form is validated and the form data is sent to local storage

$('.form_save').on('click', function (e) {
    var ID;
    count = 0;
    formData = {};
    e.preventDefault();
    formElements.each(function () {
        if (validateForm($(this))) {
            count++;
            console.log(count);
            console.log($(this));
        }
    });
    if (count === formElements.length) {
        console.log('form validated!');
        formElements.each(function () {
            formDataProperty = $(this).attr('name');
            formData[formDataProperty] = $(this).val();
        });
        ID = generateID();
        formData.id = ID;
        formData = JSON.stringify(formData);
        formData += 'next';
        if (existingWhores !== null) {
            formData += existingWhores;
            console.log(formData);
        }
        console.log(formData);
        window.localStorage.setItem('whores', formData);
        $('.add-profile-form').submit();
    } else {
        console.log('not valid');
    }
});

//Show edit/delete form with pre-filled values when a profile is clicked

$('.profile').on('click', function () {
    var editProfileForm = $('.edit-profile-form');
    currentProfile = this;
    console.log(currentProfile);
    editFormInputs = editProfileForm.find('input');
    $('.add_whore').css('display', 'none');
    oldWhores.forEach(function (element) {
        if (element.id === currentProfile.id) {
            editFormInputs.filter('.firstname').val(element.firstName);
            editFormInputs.filter('.lastname').val(element.lastName);
            editFormInputs.filter('.nickname').val(element.nickname);
            editFormInputs.filter('.age').val(element.age);
            editFormInputs.filter('.price').val(element.price);
        }
    });
    editProfileForm.css('display', 'block');
    $('.add-profile-form').css('display', 'none');
    $('.profile.active').removeClass('active');
    $(event.target).addClass('active');

    //Form validation when input loses focus

    editFormInputs.each(function () {
        $(this).on('change', function () {
            validateForm($(this));
        });

        $(this).on('focus', function () {
            $(this).removeClass('invalid');
            $(this).siblings('.error').remove();
        })
    });
});

//Save edited profile

$('.form_update').on('click', function (e) {
    e.preventDefault();
    count = 0;
    oldWhores.forEach(function (element) {
        if (element.id === currentProfile.id) {
            console.log(element.id);
            console.log(currentProfile);
            element.firstName = editFormInputs.filter('.firstname').val();
            element.lastName = editFormInputs.filter('.lastname').val();
            element.nickname = editFormInputs.filter('.nickname').val();
            element.age = editFormInputs.filter('.age').val();
            element.price = editFormInputs.filter('.price').val();
        }
    });
    editFormInputs.each(function () {
        if (validateForm($(this))) {
            count++;
            console.log(count);
        }
    });
    if (count === formElements.length) {
        console.log('form validated!');
        oldWhores.forEach(function (element) {
            updatedElement = JSON.stringify(element);
            updatedWhores.push(updatedElement);
        });
        formData = updatedWhores.join('next');
        formData += 'next';
        console.log(formData);
        window.localStorage.setItem('whores', formData);
        $('.edit-profile-form').trigger('submit');
    } else {
        console.log('not valid');
    }
});

//Delete profile

$('.form_delete').on('click', function (e) {
    e.preventDefault();
    oldWhores.forEach(function (element) {
        if (element.id !== currentProfile.id) {
            updatedElement = JSON.stringify(element);
            updatedWhores.push(updatedElement);
        }
    });

    if (updatedWhores.length !== 0) {
        formData = updatedWhores.join('next');
        formData += 'next';

    } else {
        formData = updatedWhores;
    }
    window.localStorage.setItem('whores', formData);
    $('.edit-profile-form').trigger('submit');
});

//Prevent browser from asking about form resubmission when page reloads

if ( window.history.replaceState ) {
    window.history.replaceState( null, null, window.location.href );
}
