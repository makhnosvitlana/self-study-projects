let score = {
    count: 0,
    el: document.querySelector('.score'),
    draw: function() {
      this.el.innerHTML = this.count;
    }
};

let game = {
    el: document.querySelector('.scene'),
    isOver: false,
    init: function() {
        score.draw();
        rabbit.init();
        let id = setInterval(function() {
            if (game.isOver) {
                clearInterval(id);
                return;
            }

            let obstacle = new Obstacle();
            obstacle.init();
        }, 1000);
    }
};

let Obstacle = function() {};

Obstacle.prototype = {
    x: 600,
    y: 139,
    width: 50,
    height: 100,
    el: document.querySelector('.obstacle'),
    init: function() { // creates element and calls draw + run
        this.el = document.createElement('div');
        this.el.classList.add('obstacle');
        game.el.appendChild(this.el);
        this.draw();
        this.run();
    },
    draw: function() { // sets position and size
        this.el.style.width = this.width + 'px';
        this.el.style.height = this.height + 'px';
        this.el.style.top = this.y + 'px';
        this.el.style.left = this.x + 'px';
    },
    run: function() {
        let step = 10;
        let id = setInterval(function() {
            if (this.x > -50) { // not clear?
                this.x -= step; // el.left -15 -> moves to left
                this.draw(); // sets new position closer to the left
                if (rabbit.isHit(this, rabbit)) { // if lose //correct rabbit
                    game.isOver = true;
                    game.el.classList.add('gameover');
                    clearInterval(id);
                }
            } else { // if win
                clearInterval(id);
                score.count++;
                score.draw();
                this.remove(); // the current cactus disappears and the new one appears
            }
        }.bind(this), 25)
    },
    remove: function() {
        this.el.parentNode.removeChild(this.el);
    }
};

let rabbit = {
    x: 10,
    y: 150,
    width: 75,
    height: 88,
    el: document.querySelector('.rabbit'),
    array: ['-10px', '-85px', '-160px', '-235px', '-310px', '-385px'],
    state: 0,
    inJump: false,
    init: function() {
        this.draw();
        this.subscribe();
        this.run();
    },
    draw: function() { // sets position and size
      this.el.style.width = this.width + 'px';
      this.el.style.height = this.height + 'px';
      this.el.style.top = this.y + 'px';
      this.el.style.left = this.x + 'px';
    },
    subscribe: function() {
        document.addEventListener('keydown', function (event) {
            if (event.which === 32) { // which - deprecated, substitute
                this.jump();
            }
        }.bind(this));
    },
    jump: function() {
      if (this.inJump || game.isOver) {
          return;
      }

      let counter = 35;
      let step = 17; // 16, 15...
      this.inJump = true;
      let id = setInterval(function() {
          if (game.isOver) {
              clearInterval(id);
              return;
          }

          if (counter > 0) {
              this.y -= step; // el.top - 17 -> moves up
              this.draw(); // sets new position higher
              step--; // ??
          } else {
              this.inJump = false;
              clearInterval(id);
          }
          counter--;
      }.bind(this), 25);
    },
    run: function() {
        let id = setInterval(function() {
            console.log(this.inJump);
            if (this.inJump) {
                return;
            }

            if (game.isOver) {
                clearInterval(id);
                return;
            }

            this.changeSpriteToNext();
        }.bind(this), 100);
    },
    changeSpriteToNext: function() {
        this.state++;

        if (this.state > this.array.length - 1) {
            this.state = 0;
        }

        this.el.style.backgroundPositionX = this.array[this.state];
    },
    isHit: function(cactus, rab) {
        if(
            (((rab.y > cactus.y) && (rab.y < cactus.y + cactus.height)) || ((rab.y + rab.height > cactus.y) && (rab.y + rab.height < cactus.y + cactus.height))) &&
            (((rab.x > cactus.x) && (rab.x < cactus.x + cactus.width )) || ((rab.x + rab.width  > cactus.x) && (rab.x + rab.width  < cactus.x + cactus.width )))
        ) {
            return true;
        } else {
            return false;
        }
    }
};

game.init();