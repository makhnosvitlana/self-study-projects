import React, {Component} from 'react';
import WeatherView from './WeatherView';

// weather for 5 days:
// http://api.openweathermap.org/data/2.5/forecast?q=Kiev&appid=e54ece7ec1570e9f8b3cacde0fabfc6b&units=metric

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showWeather: false,
            city: '',
            data: {},
            loading: false,
            message: '',
        };
        this.handleClick = this.handleClick.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        this.setState({
            city: event.target.value
        });
    };

    handleClick(event) {
        event.preventDefault();
        this.setState({
            showWeather: true,
            loading: true
        });
        const city = this.state.city;
        const url = "api.openweathermap.org/data/2.5/weather?q="
            + city
            + "&units=metric&appid=e54ece7ec1570e9f8b3cacde0fabfc6b";
        fetch("https://cors-anywhere.herokuapp.com/" + url)
            .then(response => response.json())
            .then(data => {
                let message = 'Weather in ';
                let response = data;
                if (data.cod !== 200) {
                    if (data.cod === '404') {
                        message = 'No such city found';
                    } else {
                        message = 'Sorry, an error occurred';
                        response = {};
                    }
                }
                this.setState({
                    loading: false,
                    message: message,
                    data: response
                })
            })
    };

    // handleClick(event) {
    //     event.preventDefault();
    //     this.setState({
    //                 showWeather: true,
    //                 loading: true
    //             });
    //     Promise.all([
    //         fetch("https://cors-anywhere.herokuapp.com/" + 'http://api.openweathermap.org/data/2.5/forecast?q=Kiev&appid=e54ece7ec1570e9f8b3cacde0fabfc6b&units=metric'),
    //         fetch("https://cors-anywhere.herokuapp.com/" + "api.openweathermap.org/data/2.5/weather?q=Berlin&units=metric&appid=e54ece7ec1570e9f8b3cacde0fabfc6b")
    //     ]).then(([r1, r2]) => {
    //
    //     }).then(data => {
    //         console.log(data);
    //     })
    // }

    render() {
        return (
            <div className="app">
                <h1>Weather in your <span  className="city-name">city</span></h1>
                <form action="" onSubmit={this.handleClick}>
                    <input type='text'
                           placeholder='Enter city name'
                           onChange={this.handleChange}
                           value={this.state.city}
                    />
                    <button type="button" onClick={this.handleClick}>Search</button>
                </form>
                {
                    !this.state.showWeather ?
                    '' : this.state.loading ?
                        'loading...' : <WeatherView data={this.state.data} message={this.state.message}/>
                }
            </div>
        )
    }
}

export default App;