import React from 'react';

function WeatherView(props) {
    return(
        <div className="weather-view">
            {props.data.name ?
                <div>
                    <h2>{props.message} <span className="city-name">{props.data.name}</span></h2>
                    <div className="weather-view_details">
                        <p>{props.data.weather[0].main}</p>
                        <p>{props.data.weather[0].description}</p>
                        <p>Humidity<br/> {props.data.main.humidity}</p>
                        <p>Temperature<br/> {Math.round(props.data.main.temp)}</p>
                        <p>Min Temperature<br/> {Math.round(props.data.main.temp_min)}</p>
                        <p>Max Temperature<br/> {Math.round(props.data.main.temp_max)}</p>
                    </div>
                </div>
                : <p>{props.message}</p>}
        </div>
        )
}

export default WeatherView;