// Write your code here

var body = document.querySelector('body');

//*********************************Task 1 -  изменить цвет текста

var changeColorBtn = document.querySelector('.b1');
changeColorBtn.addEventListener('click', function () {
    var test1 = document.querySelector('.t1');
    test1.style.color = 'green';
});

//*********************************Task 2 - изменить цвет фона

var changeBackgroundBtn = document.querySelector('.b2');
changeBackgroundBtn.addEventListener('click', function () {
    var test2 = document.querySelector('.t2');
    test2.style.background = 'green';
});

//*********************************Task 3 - изменить путь ссылки

var changeLinkPathBtn = document.querySelector('.b3');
changeLinkPathBtn.addEventListener('click', function () {
    var link1 = document.querySelector('.t3 a');
    link1.href = 'https://www.playtech.com/';
});

//*********************************Task 4 - изменить путь к картинке

var changeImgPathBtn = document.querySelector('.b4');
changeImgPathBtn.addEventListener('click', function () {
    var img1 = document.querySelector('.t4 img');
    img1.src = 'images/kat.png';
});

//*********************************Task 5 - изменить id элемента

var changeElementIdBtn = document.querySelector('.b5');
changeElementIdBtn.addEventListener('click', function () {
    var test5 = document.querySelector('.t5');
    test5.id = 'test5';
});

//*********************************Task 6 - изменить текст

var changeTextBtn = document.querySelector('.b6');
changeTextBtn.addEventListener('click', function () {
    var test6 = document.querySelector('.t6');
    test6.textContent = 'New Text';
});

//*********************************Task 7 - изменить HTML

var changeHTMLBtn = document.querySelector('.b7');
changeHTMLBtn.addEventListener('click', function () {
    var test7 = document.querySelector('.t7');
    test7.innerHTML = 'Тест Цветы';
});

//*********************************Task 8 - изменить размер шрифта

var changeFontSize = document.querySelector('.b8');
changeFontSize.addEventListener('click', function () {
    var test8 = document.querySelector('.t8');
    test8.style.fontSize = '22px';
});

//*********************************Task 9 - добавить класс 'hi'

var addClassHiBtn = document.querySelector('.b9');
addClassHiBtn.addEventListener('click', function () {
    var test9 = document.querySelector('.t9');
    test9.className = 'hi';
});

//*********************************Task 10 - удалить класс 'hi'

var deleteClassHiBtn = document.querySelector('.b10');
deleteClassHiBtn.addEventListener('click', function () {
    var test10 = document.querySelector('.t10');
    test10.classList.remove('hi');
});

//*********************************Task 11 - добавлять/удалять (в зависимости от наличия) класс 'hi'

var toggleClassHiBtn = document.querySelector('.b11');
toggleClassHiBtn.addEventListener('click', function () {
    var test11 = document.querySelector('.t11');
    test11.classList.toggle('hi');
});

//*********************************Task 12 - При клике на любом эелементе страницы выводить значение атрибута class элемента

body.addEventListener('click', function (e) {
    var test12 = document.querySelector('.t12');
    test12.innerHTML = e.target.classList;
});

//*********************************Task 13 - перевести текст на английский язык

var translateEnBtn = document.querySelector('.b13');
translateEnBtn.addEventListener('click', function () {
    var test13 = document.querySelector('.t13');
    test13.innerHTML = test13.dataset.en;
});

//*********************************Task 14 - При изменении размеров окна вкладки или браузера изменять фоновый цвет абзаца (использовать RGB)

window.addEventListener('resize', function () {
    var test14 = document.querySelector('.t99');
    test14.style.background = 'rgb(' + Math.round(Math.random() * 255) + ', 90, 90)';
});

//*********************************Task 15 - При изменении значения элемента формы выводить количество символов которое оно содержит

var test15 = document.querySelector('.t98 input');
test15.addEventListener('input', function () {
    var inputLength = document.querySelector('.t98-2');
    inputLength.innerHTML = test15.value.length;
});

//*********************************Task 16 - Вывести на экран анкету Жасмин используя переменную Jasmine

var addWhoreBtn = document.querySelector('.b14');
var newWhore = document.createElement('div');
var newWhore2;
var createWhores = function () {

    var name2 = '<div class="whore-name">' + Jasmine.name + ' 2' + '</div>';
    var img2 = '<img src="' + Jasmine.photo + '" width="200">';
    var age2 = '<div class="whore-age">Возраст: ' + Jasmine.age + '</div>';
    var boobs2 = '<div class="whore-boobs">Размер груди: ' + Jasmine.boobs + '</div>';
    var height2 = '<div class="whore-height">Рост: ' + Jasmine.height + '</div>';
    var weight2 = '<div class="whore-weight">Вес: ' + Jasmine.weight + '</div>';
    var phone2 = '<div class="whore-phone">' + Jasmine.phone + '</div>';
    var canCome2 = '<div class="whore-can-come">Выезд: -</div>';
    var teaser2 = '<div class="whore-teaser">' + Jasmine.teaser + '</div>';

    var name = document.createElement('div');
    var img = document.createElement('img');
    var age = document.createElement('div');
    var boobs = document.createElement('div');
    var height = document.createElement('div');
    var weight = document.createElement('div');
    var phone = document.createElement('div');
    var canCome = document.createElement('div');
    var teaser = document.createElement('div');
    name.classList.add('whore-name');
    name.textContent = Jasmine.name;
    img.style.width = '200px';
    img.src = Jasmine.photo;
    age.classList.add('whore-age');
    age.textContent = 'Возраст: ' + Jasmine.age;
    boobs.classList.add('whore-boobs');
    boobs.textContent = 'Размер груди: ' + Jasmine.boobs;
    height.classList.add('whore-height');
    height.textContent = 'Рост: ' + Jasmine.height;
    weight.classList.add('whore-weight');
    weight.textContent = 'Вес: ' + Jasmine.weight;
    phone.classList.add('whore-phone');
    phone.textContent = Jasmine.phone;
    canCome.classList.add('whore-can-come');
    if (Jasmine.can_come) {
        canCome.textContent = 'Выезд: -';
    }
    teaser.classList.add('whore-teaser');
    teaser.textContent = Jasmine.teaser;
    newWhore.classList.add('whore');
    newWhore.append(name, img, age, boobs, height, weight, phone, canCome, teaser);
    newWhore2 = '<div class="whore">' + name2 + img2 + age2 + boobs2 + height2 + weight2 + phone2 + canCome2 + teaser2 + '</div>';
};

var whoreDot = doT.template('<div class="whore">' +
    '<div class="whore-name">{{= it.name}} 3</div>' +
    '<img src="{{= it.photo}}" width="200">' +
    '<div class="whore-age">Возраст: {{= it.age}}</div>' +
    '<div class="whore-boobs">Размер груди: {{= it.boobs}}</div>' +
    '<div class="whore-height">Рост: {{= it.height}}</div>' +
    '<div class="whore-weight">Вес: {{= it.weight}}</div>' +
    '<div class="whore-phone">{{= it.phone}}</div>' +
    '<div class="whore-can-come">Выезд: {{? it.can_come}}+{{??}}-{{?}}</div>' +
    '<div class="whore-teaser">{{= it.teaser}}</div>' +
    '</div>');

addWhoreBtn.addEventListener('click', function () {
    var whores = document.querySelector('.whores-container');
    createWhores();
    whores.append(newWhore);
    whores.innerHTML += newWhore2;
    whores.innerHTML += whoreDot(Jasmine);
});


//*********************************Task 17 - Переместить рыбу из первого контейнера во второй (при повторном клике из второго в первый и т.д.)

var moveFishBtn = document.querySelector('.b15');
moveFishBtn.addEventListener('click', function () {
    var fish = document.querySelector('.fish');
    var container1 = document.querySelector('.cat-container-1');
    var container2 = document.querySelector('.cat-container-2');

    if (document.querySelector('.cat-container-1 .fish')) {
        container2.appendChild(fish);
    } else {
        container1.appendChild(fish);
    }

    // var withoutFish = container1.contains(fish) ? container2 : container1;
    // var withFish = container1.contains(fish) ? container1 : container2;
    // withFish.removeChild(fish);
    // withoutFish.appendChild(fish);
});

//*********************************Task 18 - Удалить зуб

var removeToothBtn = document.querySelector('.b16');
removeToothBtn.addEventListener('click', function () {
    var tooth = document.querySelector('.tooth');
    var toothContainer = document.querySelector('.tooth-container');
    toothContainer.removeChild(tooth);
});

//*********************************Task 19 - Хочу чтоб лыжник бесконечно ехал вправо (сдвиг на 5px каждые 16ms). При нажатии на кнопку "Стоп!" останавливался.

var goBtn = document.querySelector('.b17');
goBtn.addEventListener('click', function () {
    var skier = document.querySelector('.skier');
    var currentPos = 0;
    var intervalId = setInterval(function () {
        currentPos += 5;
        skier.style.left = currentPos + 'px';
    }, 16);

    var btnStop = document.querySelector('.b17-2');
    btnStop.addEventListener('click', function () {
        clearInterval(intervalId);
    });
});


//*********************************Task 20 - copy Mikki

var createMikki = function (mikki) {
    var mikki_tiles = document.querySelector('.mikki_tiles');
    var i;
    mikki.forEach(function (element) {
        for (i = 0; i < element.length; i++) {
            if (element[i] === 'X') {
                mikki_tiles.innerHTML += '<div class="mikki_tile" style="background: black;"></div>';
            } else {
                mikki_tiles.innerHTML += '<div class="mikki_tile"></div>';
            }
        }
    })
};
createMikki(mikki);

//*********************************Task 21 - Создать мир Марио

var createMarioWorldBtn = document.querySelector('.b18');
createMarioWorldBtn.addEventListener('click', function () {
    var scene = document.querySelector('.scene');
    var i;
    var j;
    var newTile;
    var tileClass;

    for (i = 0; i < map.length; i++) {
        for (j = 0; j < map[i].length; j++) {
            newTile = document.createElement('div');
            newTile.classList.add('tile');
            newTile.style.left = j * 16 + 'px';
            newTile.style.top = i * 16 + 'px';

            if (map[i][j] !== ' ') {
                tileClass = 'x_' + map[i][j];
                newTile.classList.add(tileClass);
            }
            scene.appendChild(newTile);
        }
    }
});

//*********************************Task 22

var previousBtn = document.querySelector('.previous');
var nextBtn = document.querySelector('.next');

nextBtn.addEventListener('click', function () {
    var slider = document.querySelector('.slider');
    var currentActive = document.querySelector('.slider .active');
    if (!(slider.lastElementChild.classList.contains('active'))) {
        currentActive.classList.remove('active');
        currentActive.nextElementSibling.classList.add('active');
    }
});

previousBtn.addEventListener('click', function () {
    var slider = document.querySelector('.slider');
    var currentActive = document.querySelector('.slider .active');
    if (currentActive !== slider.firstElementChild) {
        currentActive.previousElementSibling.classList.add('active');
        currentActive.classList.remove('active');
    }
});

//*********************************Task 23

var questions = document.querySelectorAll('.question');
questions.forEach(function (element) {
    element.addEventListener('click', function () {
        this.classList.toggle('active');
    });
});


//*********************************Task 24

var tabs = document.querySelectorAll('.x-tab');
var items = document.querySelectorAll('.x-item');

var showMovies = function () {
    var day = this.dataset.tab;
    document.querySelector('.x-tab.active').classList.remove('active');
    this.classList.add('active');
    document.querySelector('.x-item.active').classList.remove('active');
    items.forEach(function (element) {
        if (element.dataset.section === day) {
            element.classList.add('active');
        }
    })
};

tabs.forEach(function (element) {
    element.addEventListener('click', showMovies)
});


//*********************************Task 25

var loginBtn = document.querySelector('.show-login-pop-up');
loginBtn.addEventListener('click', function () {
    var closeBtn = document.querySelector('.close');
    var overlay = document.querySelector('.overlay');
    var loginPopup = document.querySelector('.pop-up.pop-up--login');
    overlay.classList.remove('hidden');
    loginPopup.classList.remove('hidden');
    closeBtn.addEventListener('click', function () {
        overlay.classList.add('hidden');
        loginPopup.classList.add('hidden');
    })
});

//*********************************Task 26

var linkEbanoe = document.querySelector('.link-ebanoe');
linkEbanoe.addEventListener('click', function (e) {
    e.preventDefault();
});


// ********************************Task 27

var ad = document.querySelector('.t97');
var hideAd = function () {
    ad.style.display = 'none';
    body.style.paddingTop = '0';
};

var checkAd = function () {
    if (localStorage.getItem('Hide Ad')) {
        hideAd();
    }
};

document.querySelector('.b97').addEventListener('click', function () {
    hideAd();
    window.localStorage.setItem('Hide Ad', 'true');
});

window.addEventListener('load', checkAd);

// ********************************Task 28
//Кликнув на кнопку "Load more" необходимо отправить HTTP GET запрос по адресу 'http://127.0.0.1:3000/'
// и получив ответ в виде массива игр добавить их в контейнер

var loadMoreBtn = document.querySelector('.load-more');
var gamesContainer = document.querySelector('.games');
var tempFn = doT.template(document.querySelector('#game-template').innerHTML);

loadMoreBtn.addEventListener('click', function () {
    var gamesResponse;
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'http://127.0.0.1:3000/', true);
    xhr.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            var games = JSON.parse(this.responseText);
            games.forEach(function (game) {
                gamesContainer.innerHTML += tempFn(game);
            });
        }
    };
    xhr.send();
});

//***********************************Task 29 - Валидация формы

var validateForm = function () {
    var form = document.querySelector('.form');
    var validateAll;
    var validate;
    var formElements = document.querySelectorAll('.field');
    var fieldset = document.querySelector('.fieldset');
    var emailPattern = /^(?=.*@)(?=.*\.).*$/;
    var usernamePattern = /^[a-zA-Z]{3,10}$/;
    var passwordPattern = /^[a-zA-Z0-9]{8,20}$/;
    var firstNamePattern = /^[a-zA-Z]{3,15}$/;
    var lastNamePattern = /^[a-zA-Z]{4,20}$/;
    var addressPattern = /^[a-zA-Z]{4,30}$/;
    var cityPattern = /^[a-zA-Z]{3,16}$/;
    var zipPattern = /^[0-9]{5}$/;
    var phonePattern = /^[0-9\\+1]{13}$/;
    var promoPattern = /^[a-zA-Z0-9]{10}$/;
    var error;
    var input;
    var value;
    var formData = {};
    var errorElement = document.createElement('div');
    errorElement.classList.add('.field__error');

    validate = function (pattern, value) {
        if (!value.match(pattern)) {
            errorElement.innerText = error;
            return false;
        } else {
            return true;
        }
    };

    validateAll = function (element) {
        input = element.querySelector('input') || element.querySelector('select');
        if (input === element.querySelector('input')) {
            value = element.querySelector('input').value;
        } else if (input === element.querySelector('select')) {
            value = element.querySelector('select').value;
        }

        if (input.name === 'email') {
            error = 'Email must contain @ and .';
            if (!validate(emailPattern, value)) {
                element.classList.add('invalid');
                element.append(errorElement);
                return false;
            } else {
                return true;
            }
        }

        if (input.name === 'userName') {
            error = 'Username must contain 3 to 10 letters';
            if (!validate(usernamePattern, value)) {
                element.classList.add('invalid');
                element.append(errorElement);
                return false;
            } else {
                return true;
            }
        }

        if (input.name === 'password') {
            error = 'Password must contain 8 to 20 letters and/or numbers';
            if (!validate(passwordPattern, value)) {
                element.classList.add('invalid');
                element.append(errorElement);
                return false;
            } else {
                return true;
            }
        }

        if (input.name === 'firstname') {
            error = 'First name must contain 3 to 15 letters';
            if (!validate(firstNamePattern, value)) {
                element.classList.add('invalid');
                element.append(errorElement);
                return false;
            } else {
                return true;
            }
        }

        if (input.name === 'lastname') {
            error = 'Last name must contain 3 to 20 letters';
            if (!validate(lastNamePattern, value)) {
                element.classList.add('invalid');
                element.append(errorElement);
                return false;
            } else {
                return true;
            }
        }

        if (input.name === 'address') {
            error = 'Address must contain 4 to 30 letters';
            if (!validate(addressPattern, value)) {
                element.classList.add('invalid');
                element.append(errorElement);
                return false;
            } else {
                return true;
            }
        }

        if (input.name === 'city') {
            error = 'City must contain 3 to 16 letters';
            if (!validate(cityPattern, value)) {
                element.classList.add('invalid');
                element.append(errorElement);
                return false;
            } else {
                return true;
            }
        }

        if (input.name === 'zip') {
            error = 'Zip must be a 5-digit string';
            if (!validate(zipPattern, value)) {
                element.classList.add('invalid');
                element.append(errorElement);
                return false;
            } else {
                return true;
            }
        }

        if (input.name === 'phonenumber') {
            error = 'Phone number must contain a "+" sign and 12 digits';
            if (!validate(phonePattern, value)) {
                element.classList.add('invalid');
                element.append(errorElement);
                return false;
            } else {
                return true;
            }
        }

        if (input.name === 'coupon') {
            error = 'Promo code must contain 10 characters';
            if (value !== '') {
                if (!validate(promoPattern, value)) {
                    element.classList.add('invalid');
                    element.append(errorElement);
                    return false;
                }
            }
            return true;
        }

        if (input.name === 'sex' || input.name === 'day' || input.name === 'month' || input.name === 'year' || input.name === 'subscription' || input.name === 'terms' && value !== '') {
            return true;
        }
    };

    //Все поля помеченные символом * обязательны для заполнения

    formElements.forEach(function (element) {
        if (element.innerHTML.includes('*')) {
            element.querySelector('input').required = true;
        }
    });

    if (fieldset.innerHTML.includes('*')) {
        fieldset.querySelectorAll('select').forEach(function (element) {
            element.required = true;
        })
    }

    //Если элемент формы получает фокус необходимо добавить класс "focused" к элементу с классом "field".

    formElements.forEach(function (element) {
        element.addEventListener('focus', function () {
            element.classList.add('focused');
            element.classList.remove('invalid');
            if (element.contains(errorElement)) {
                element.removeChild(errorElement);
            }
        })
    });

    //При потере фокуса убрать класс "focused"
    //Валидация поля происходит при потере фокуса элементом формы

    formElements.forEach(function (element) {
        element.addEventListener('focusout', function () {
            element.classList.remove('focused');
            validateAll(element);
        })
    });

    //Валидация поля происходит по нажатию на кнопку "Register"
    form.addEventListener('submit', function (e) {
        var count = 0;
        var formDataProperty;
        e.stopPropagation();
        formElements.forEach(function (element) {
            console.log(validateAll(element));
            if (validateAll(element)) {
                count++;
            }
        });
        if (count === formElements.length) {
            console.log('validated!');
            formElements.forEach(function (element) {
                formDataProperty = element.querySelector('[name]').name;
                formData[formDataProperty] = element.querySelector('[name]').value;
                console.log(element.querySelector('[name]').value);
            });
            //В случае если валидация пройдена, сформировать из данных полей формы JSON и сохранить его в localStorage в виде строки
            formData = JSON.stringify(formData);
            window.localStorage.setItem('FormData', formData);

        } else {
            console.log('not valid');
            e.preventDefault();
        }
    })
};

validateForm();


//***********************************Task 30 - Прочитать данные из localStorage (сохраненные туда в прошлом задании) и вывести их используя шаблонизатор.

var showFormData = function () {
    var formData = JSON.parse(window.localStorage.getItem('FormData'));
    var formDataContainer = document.querySelector('.form-data-container');
    var template = document.querySelector('#form-data-template');
    var formTemplate = doT.template(template.innerHTML);
    formDataContainer.innerHTML = formTemplate(formData);
};

showFormData();

//***********************************Task 31 - Вывести меню по-умолчанию на английском языке и реализовать смену языка посредством кнопок

var showMenu = function (translationArray) {
    var tempFn;
    var navContainer = document.querySelector('.nav');
    var navLinksObj = {};
    navLinksObj.links = translationArray;
    tempFn = doT.template('{{~it.links :link :index}}' +
        '        <a href="{{= link.url}}">{{= link.name}}</a>' +
        '        {{~}}');
    navContainer.innerHTML = tempFn(navLinksObj);
};

showMenu(navLinksEn);

var showMenuEnBtn = document.querySelector('.btn-nav-en');
var showMenuRoBtn = document.querySelector('.btn-nav-ro');

showMenuRoBtn.addEventListener('click', function () {
    showMenu(navLinksRo);
});
showMenuEnBtn.addEventListener('click', function () {
    showMenu(navLinksEn);
});
