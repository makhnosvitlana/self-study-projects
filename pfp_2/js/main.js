var maximizeBtn = document.querySelector(".wishlist__resize");

var overlay = document.querySelector('.overlay');

var wishlist = document.querySelector('.wishlist');

maximizeBtn.addEventListener('click', function () {
    overlay.style.display = 'block';
    wishlist.classList.add('wishlist--overlay');
    overlay.append(wishlist);
    maximizeBtn.innerText = 'Minimize';
    maximizeBtn.addEventListener('click', function () {
        console.log('minimize clicked');
        overlay.style.display = 'none';
        document.querySelector('.page').append(wishlist);
        wishlist.classList.remove('wishlist--overlay');
    })
});