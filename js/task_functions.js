// Реализовать нижеуказанные функции.
// ВНИМАНИЕ!
// Соблюдайте форматирование кода (отступы, переносы)
// Не использовать встроенные функции/методы


// Создать функцию isUndefined которая принимает в качестве единственного входящего параметра значение любого типа данных
// и возвращает true если значение равно undefined иначе возвращает false.
// Пример работы:
// isUndefined(undefined);
// => true
// isUndefined('hello');
// => false

var isUndefined = function(value) {
    return value === undefined;
};

// Создать функцию isNull которая принимает в качестве единственного входящего параметра значение любого типа данных
// и возвращает true если значение равно null иначе возвращает false.
// Пример работы:
// isNull(null);
// => true

var isNull = function(value) {
    return value === null;
};

// Создать функцию isBoolean которая принимает в качестве единственного входящего параметра значение любого типа данных
// и возвращает true если принимаемое значение является значением логического типа данных. В противном случае вернет false.
// Пример работы:
// isBoolean(null);
// => false
// isBoolean(false);
// => true

var isBoolean = function(value) {
    return typeof value === "boolean";
};

// Создать функцию size которая принимает в качестве единственного входящего параметра массив и возвращает количество элементов в массиве.
// Пример работы:
// size([7, 2, 3, 5, 5, 3]);
// => 6

var size = function(array) {
    return array.length;
};

// Создать функцию first которая принимает в качестве единственного входящего параметра массив произвольных значений и возвращает первое из них.
// Пример работы:
// first([5, 4, 3, 2, 1]);
// => 5

var first = function(array) {
    return array[0];
};

// Создать функцию last которая принимает в качестве единственного входящего параметра массив произвольных значений и возвращает последнее из них.
// Пример работы:
// last([5, 4, 3, 2, 1]);
// => 1

var last = function(array) {
    return array[array.length - 1];
};

// Создать функцию getPositiveNumbers которая принимает массив чисел и возвращает массив положительных чисел найденных в исходном массиве.
// Пример работы:
// getPositiveNumbers([10, -5, 100, -2, 1000]);
// => [10, 100, 1000]

var getPositiveNumbers = function(array) {
    var i;
    var positiveNumbers = [];
    for (i = 0; i < array.length; i++) {
        if (array[i] > 0) {
            positiveNumbers[positiveNumbers.length] = array[i];
        }
    }
    return positiveNumbers;
};

// Создать функцию isEven которая возвращает true если число четное или false в противном случае.
// Пример работы:
// isEven(10);
// => true
// isEven(8);
// => true
// isEven(7);
// => false

var isEven = function(number) {
    return number % 2 === 0;
};

// Создать функцию push которая принимает массив и произвольное значение и возвращает копию массива + произвольное значение (которое находится в конце массива)
// Пример работы:
// push([1, 2, 3, 4], 5);
// => [1, 2, 3, 4, 5]

var push = function(array, newValue) {
    array[array.length] = newValue;
    return array;
};

// Создать функцию unshift которая принимает массив и произвольное значение и возвращает копию массива + произвольное значение (которое находится в начале массива)
// Пример работы:
// unshift([1, 2, 3, 4], 5);
// => [5, 1, 2, 3, 4]

var unshift = function(array, newValue) {
    var i;
    var newArray = [];
    newArray[newArray.length] = newValue;
    for (i = 0; i < array.length; i++) {
        newArray[newArray.length] = array[i];
    }
    return newArray;
};

// Создать функцию pop которая принимает массив и возвращает копию массива без последнего значения.
// Пример работы:
// pop([1, 2, 3, 4]);
// => [1, 2, 3]

var pop = function(array) {
    var newArray = [];
    var i;
    for (i = 0; i < array.length-1; i++){
        newArray[newArray.length] = array[i];
    }
    return newArray
};

// Создать функцию shift которая принимает массив и возвращает копию массива без первого значения.
// Пример работы:
// shift([1, 2, 3, 4]);
// => [2, 3, 4]

var shift = function(array) {
    var newArray = [];
    var i;
    for (i = 1; i < array.length; i++) {
        newArray[newArray.length] = array[i];
    }
    return newArray;
};

// Создать функцию compact которая принимает в качестве единственного входящего параметра массив произвольных значений и возвращает копию массива без undefined значений.
// Пример работы:
// compact([10, 1, 4, 2, undefined, 3]);
// => [10, 1, 4, 2, 3]

var compact = function(array) {
    var newArray = [];
    var i;
    for (i = 0; i < array.length; i++) {
        if (array[i] !== undefined) {
            newArray[newArray.length] = array[i];
        }
    }
    return newArray;
};

// Создать функцию contains которая принимает два входящих параметра (массив значений простых типов данных и значение простого типа данных).
// Функция вернет true если в массиве содержится определенное значение. Иначе функция вернет false.
// Пример работы:
// contains([1, 2, 3], 3);
// => true

var contains = function(array, checkValue) {
    var i;
    for(i = 0; i < array.length; i++) {
        if(array[i] === checkValue) {
            return true;
        }
    }
    return false;
};

// Создать функцию without которая возвращает копию массива, в которой удалены все значения второго аргумента указанного при вызове функции.
// Пример работы:
// without([3, 6, 7, 'rere'], 6);
// => [3, 7, 'rere']

var without = function(array, removeValue) {
    var newArray = [];
    var i;
    for (i = 0; i < array.length; i++) {
        if (array[i] !== removeValue) {
            newArray[newArray.length] = array[i];
        }
    }
    return newArray;
};

// Создать функцию uniq которая принимает массив элементов и возвращает новый массив на основе входящего, где все значения уникальны.
// Пример работы:
// uniq([1, 2, 1, 4, 1, 3]);
// => [1, 2, 4, 3]

var uniq = function(array) {
    var newArray = [];
    var i;
    var j;
    newArray[newArray.length] = array[0];
    for (i = 1; i < array.length; i++) {
        for (j = 0; j < newArray.length; j++) {
            if (newArray[j] === array[i]) {
                break;
            } else if (newArray[j] !== array[i] && j === newArray.length - 1) {
                newArray[newArray.length] = array[i];
            }
        }
    }
    return newArray;
};

// Создать функцию intersection которая принимает два массива и возвращает массив элементов, встречающихся в каждом из переданных массивов. Значения не должны повторяться.
// Пример работы:
// intersection([1, 2, 3], [101, 2, 1, 2]);
// => [1, 2]

var intersection = function(array1, array2) {
    var newArray = [];
    var i;
    var j;
    var smallerArray = array1.length < array2.length ? array1 : array2;
    var largerArray = array1.length > array2.length ? array1 : array2;
    for (i = 0; i < smallerArray.length; i++) {
        for (j = 0; j < largerArray.length; j++) {
            if (smallerArray[i] === largerArray[j]) {
                newArray[newArray.length] = smallerArray[i];
                break;
            }
        }
    }
    return newArray;
};

// Создать функцию reverse которая принимает массив и возвращает копию входящего массива с элементами в обратном порядке.
// Пример работы:
// reverse([1, 'lol', 5, {}, []]);
// => [[], {}, 5, "lol", 1]

var reverse = function(array) {
    var newArray = [];
    var i = array.length - 1;
    for (i; i >= 0; i--) {
        newArray[newArray.length] = array[i];
    }
    return newArray
};

// Создать функцию join которая принимает массив и возвращает строку состоящую из его элементов разделенных запятой (по-умолчанию) или любым другим разделителем указанным во втором аргументе вызываемой функции.
// Пример работы:
// join([1, 'lol', 5, 'dro']);
// => "1,lol,5,dro"
// join([1, 'lol', 5, 'dro'], '+');
// => "1+lol+5+dro"

var join = function(array, separator) {
    var separator = separator || ',';
    var string = '';
    var i;
    for (i = 0; i < array.length; i++) {
        if (i === array.length - 1) {
            string += array[i];
        } else {
            string += array[i] + separator;
        }
    }
    return string;
};

// Создать функцию indexOf которая вернёт позицию, на которой находится элемент value в массиве array, или -1, если данный элемент не был найден.
// Пример работы:
// indexOf([77, 2, 3], 2);
// => 1

var indexOf = function(array, searchValue) {
    var i;
    for (i = 0; i < array.length; i++) {
        if (array[i] === searchValue) {
            return i;
        }
    }
    return -1;
};

// Создать функцию lastIndexOf которая паринимает два параметра (массив, значение) и ищет значение в массиве и возвращет его индекс, но делает это не с начала массива, а с его конца.
// Т.е. возвращает позицию последнего вхождения значения в массиве значений. Иначе возвращает -1.
// Пример работы:
// lastIndexOf([1, 2, 3, 1, 2, 3], 2);
// => 4

var lastIndexOf = function(array, searchValue) {
    var i;
    for(i = array.length - 1; i >= 0; i--) {
        if (array[i] === searchValue) {
            return i;
        }
    }
    return -1;
};

// Создать функцию concat которая принимает два массива и возвращает новый массив состоящий из значений первого и второго.
// Пример работы:
// concat(['a', 'b', 'c'], ['d', 'e', 'f']);
// => [ "a", "b", "c", "d", "e", "f" ]

var concat = function(array1, array2) {
    var i;
    var j;
    var newArray = [];
    for (i = 0; i < array1.length; i++) {
        newArray[newArray.length] = array1[i];
    }
    for (j = 0; j < array2.length; j++){
        newArray[newArray.length] = array2[j];
    }
    return newArray;
};

// Создать функцию slice которая принимает 3 параметра. Массив и два числа (begin и end). Возвращает копию части исходного массива.
// Начиная с индекса begin и заканчивая индексом end включительно (или концом массива если параметр end отстутствует).
// Пример работы:
// slice(['ant', 'bison', 'camel', 'duck', 'elephant'], 2, 3);
// => ['camel', 'duck']

var slice = function(array, begin, end) {
    var i;
    var newArray = [];
    if (end && end < array.length) {
        for (i = begin; i <= end; i++) {
            newArray[newArray.length] = array[i];
        }
    } else {
        for (i = begin; i < array.length; i++) {
            newArray[newArray.length] = array[i];
        }
    }
    return newArray;
};

// Создать функцию splice которая принимает 4 параметра. Массив, два числа (start и deleteCount) и массив.
// Возвращает копию части первого массива добавляя элементы второго массива начиная с индекса start.
// Параметр deleteCount это количество элементов которые необходимо удалить начиная с индекса start.
// Пример работы:
// splice(['Jan', 'March', 'April', 'June'], 1, 0, ['Feb']);
// => ['Jan', 'Feb', 'March', 'April', 'June']
// splice(['Jan', 'Feb', 'March', 'April', 'June'], 4, 1, ['May', 'June', 'July']);
// => ['Jan', 'Feb', 'March', 'April', 'May', 'June', 'July']

var splice = function(array, start, deleteCount, array2) {
   var newArray = [];
   var i;
   for (i = 0; i < start; i++) {
       newArray[newArray.length] = array[i];
   }
   for (i = 0; i < array2.length; i++) {
       newArray[newArray.length] = array2[i];
   }
   for (i = start + deleteCount; i < array.length; i++) {
       newArray[newArray.length] = array[i];
   }
   return newArray;
};

// Создать функцию getMatrixSum которая принимает матрицу чисел и возвращает сумму всех чисел.
// Пример работы:
// getMatrixSum([[1, 2], [0, 4], [1, 2]]);
// => 10

var getMatrixSum = function(array) {
    var i;
    var sum = 0;
    var j;
    for (i = 0; i < array.length; i++) {
        for(j = 0; j < array[i].length; j++) {
            sum += array[i][j];
        }
    }
    return sum;
};

// Создать функцию getMatrixSumByDiagonal которая принимает квадратную матрицу чисел и возвращает сумму чисел по диагонали (слева направо, сверху вниз).
// Пример работы:
// var matrix = [
//      [1, 2, 3],
//      [3, 0, 4],
//      [0, 1, 2]
// ];
// getMatrixSumByDiagonal(matrix);
// => 3

var getMatrixSumByDiagonal = function(array) {
    var i;
    var sum = 0;
    for (i = 0; i < array.length; i++){
        sum += array[i][i];
    }
    return sum;
};

// Создать функцию transposeMatrix которая принимает матрицу и возвращает транспонированную матрицу (Транспонированная матрица — матрица полученная из исходной матрицы заменой строк на столбцы).
// Пример работы:
// var matrix = [
//      [1, 1, 1],
//      [2, 2, 2],
//      [3, 3, 3]
// ];
// transposeMatrix(matrix);
// => [
//      [1, 2, 3],
//      [1, 2, 3],
//      [1, 2, 3]
// ]

var transposeMatrix = function(array) {
    var newArray = [];
    var i = 0;
    var j;
    var currentItem;
    currentItem = array[i];
    for (i; i < currentItem.length; i++) {
        newArray[newArray.length] = [];
    }
    for (j = 0; j < currentItem.length; j++) {
        for (i = 0, i = 0; i < array.length; i++) {
            newArray[j][i] = array[i][j];
        }
    }
    return newArray;
};

/* var matrix = [
      [1, 1, 1],
      [2, 2, 2],
      [3, 3, 3]
 ];

var matrix2 = [
    [1, 1, 1, 1],
    [2, 2, 2, 2],
    [3, 3, 3, 3],
    [0, 0, 0, 0],
    [5, 5, 5, 5]
]; */

// Создать функцию min которая принимает в качестве единственного входящего параметра массив чисел и возвращает наименьшее из них.
// Пример работы:
// min([10, 5, 100, 2, 1000]);
// => 2

var min = function(array) {
    var min = array[0];
    var i;
    for (i = 1; i < array.length; i++) {
        min = min < array[i] ? min : array[i];
    }
    return min;
};

// Создать функцию max которая принимает в качестве единственного входящего параметра массив чисел и возвращает наибольшее из них.
// Пример работы:
// max([10, 5, 100, 2, 1000]);
// => 1000

var max = function(array) {
    var current;
    var i;
    current = array[0] > array[1] ? array[0] : array[1];
    for (i = 2; i < array.length; i++) {
        current = current > array[i] ? current : array[i];
    }
    return current;
};

// Создать функцию sort которая принимает массив чисел и возвращает новый массив где числа (входящего массива) отсортированны в порядке возрастания.
// Двумя способами: Сортировка пузырьком и Сортировка выбором.
// Пример работы:
// sort([1, 10, 21, 2]);
// => [1, 2, 10, 21]

var sort = function(array) {
    var i;
    var j;
    var swap;
    for (i = 0; i < array.length-1; i++) {
        for (j = 0; j < array.length - 1 - i; j++) {
            if (array[j] > array[j + 1]) {
                swap = array[j];
                array[j] = array[j + 1];
                array[j + 1] = swap;
            }
        }
    }
    return array;
};

var selectionSort = function(array) {
    var temp;
    var i;
    var j;
    var min;
    for(i = 0; i < array.length; i++){
        min = i;
        for(j = i + 1; j < array.length; j++) {
            if(array[j] < array[min])
                min = j;
        }
        temp = array[i];
        array[i] = array[min];
        array[min] = temp;
    }
    return array;
};

// Создать функцию charAt которая принимает строку и индекс и возвращает указанный символ из строки.
// Пример работы:
// charAt('March', 0);
// => 'M'

var charAt = function(string, index) {
  return string[index];
};

// Создать функцию repeat которая принимает строку и число count и возвращает новую строку, содержащую указанное количество соединённых вместе копий строки.
// Пример работы:
// repeat('Work', 6);
// => 'WorkWorkWorkWorkWorkWork'

var repeat = function(string, count) {
    var newString = '';
    var i = 0;
    while (i < count) {
        newString += string;
        i++;
    }
    return newString;
};

// Создать функцию trim которая удаляет пробельные символы с начала и конца строки.
// Пример работы:
// trim('   Hello world!   ');
// => 'Hello world!'

var trim = function(string) {
   var i;
   var firstSymbol;
   var lastSymbol;
   var newString = '';
   for (i = 0; i < string.length; i++) {
       if (string[i] !== ' ') {
           firstSymbol = i;
           break;
       }
   }
   for (i = string.length - 1; i > firstSymbol; i--) {
       if (string[i] !== ' ') {
           lastSymbol = i;
           break;
       }
   }
   for (i = firstSymbol; i <= lastSymbol; i++) {
       newString += string[i];
   }
   return newString;
};

// Создать функцию replace которая принимает 3 параметра (строку и две подстроки).
// Возвращает новую строку с заменой первой подстроки на вторую. Только первое найденное вхождение.
// Пример работы:
// replace('The quick brown fox jumped over the lazy dog.', 'dog', 'ferret');
// => 'The quick brown fox jumped over the lazy ferret.'

var replace = function(string, replaceSubstring, insertSubstring) {
    var newString = '';
    var array = split(string, ' ');
    var i;
    var j;
    var count = 0;
    for (i = 0; i < array.length; i++) {
        for (j = 0; j < replaceSubstring.length; j++) {
            if (array[i][j] === replaceSubstring[j]) {
                count++;
            } else {
                break;
            }
        } if (count === replaceSubstring.length) {
            array[i] = insertSubstring;
            count = 0;
        }
    }
    newString = join(array, ' ');
    return newString;
};

/*
var split = function(string, separator) {
    var array = [];
    var currentString = '';
    var separatorIndex = -1;
    var i;

    for (i = separatorIndex + 1; i <= string.length; i++) {
        if (i === string.length) {
            array[array.length] = currentString;
            break;
        }
        else if (string[i] !== separator) {
            currentString += string[i];
            separatorIndex = i;
            continue;
        }
        array[array.length] = currentString;
        currentString = '';
    }
    return array;
};

var join = function(array, separator) {
    var string = '';
    var i;
    for (i = 0; i < array.length; i++) {
        if (i === array.length - 1) {
            string += array[i];
        } else {
            if (separator) {
                string += array[i] + separator;
            } else {
                string += array[i] + ','
            }
        }
    } return string;
 };

*/

//the solution could be simpler but this additional check is needed since the last array item can contain symbols like . ? ! etc
//the split and join are custom functions;

// Создать функцию split которая разбивает строку на массив строк путём разделения строки указанной подстрокой.
// Пример работы:
// split('2018-12-01', '-');
// => ["2018", "12", "01"]
// split('Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec', ',');
// => ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]

var split = function(string, separator) {
    var array = [];
    var currentString = '';
    var separatorIndex = -1;
    var i;
    for (i = separatorIndex + 1; i <= string.length; i++) {
        if (i === string.length) {
            array[array.length] = currentString;
            break;
        }
        else if (string[i] !== separator) {
            currentString += string[i];
            separatorIndex = i;
            continue;
        }
        array[array.length] = currentString;
        currentString = '';
    }
    return array;
};

// Создать функцию sum которая принимает массив чисел и возвращает их сумму.
// Пример работы:
// sum([2, 2, 3]);
// => 7

var sum = function(array) {
  var sum = 0;
  var i;
  for (i = 0; i < array.length; i++) {
      sum += array[i];
  }
  return sum;
};

// Создать функцию sumOfAllArguments которая принимает произвольное количество чисел и возвращает их сумму.
// Пример работы:
// sumOfAllArguments(2, 2, 3);
// => 7
// sumOfAllArguments(2, 2, 3, 3, 10);
// => 20

var sumOfAllArguments = function() {
  var sum = 0;
  var i;
  for (i = 0; i < arguments.length; i++) {
      sum += arguments[i];
  }
  return sum;
};

// Создать функцию sum как наследуемый метод массива который возвращает сумму своих числовых элементов.
// Пример работы:
// [2, 2, 3].sum();
// => 7
// [2, 2, 3, 3, 10].sum();
// => 20

Array.prototype.sum = function() {
    var sum = 0;
    var i;
    for (i = 0; i < this.length; i++) {
        if (typeof this[i] === "number") {
            sum += this[i];
        }
    }
    return sum;
};

// Создать функцию multiply которая принимает массив чисел и возвращает их произведение.
// Пример работы:
// multiply([2, 2, 3]);
// => 12

var multiply = function(array) {
    var product = 1;
    var i;
    for (i = 0; i < array.length; i++) {
      product *= array[i];
    }
    return product;
};

// Создать функцию abs которая принимает число и возвращает его абсолютное значение.
// Пример работы:
// abs(-4);
// => 4

var abs = function(number) {
    if (number < 0) {
        return -number;
    }
    return number;
};

// Создать функцию pow которая принимает два числа и возводит первое число в степень (представленную вторым числом).
// Пример работы:
// pow(2, 2);
// => 4
// pow(3, 3);
// => 27

var pow = function(number, power) {
    var product = 1;
    var i;
    if (power >= 0) {
        for (i = 0; i < power; i++) {
            product *= number;
        }
    } else {
        product = 1/pow(number, -power);
    }
    return product;
};

// Создать функцию round которая возвращает число, округлённое к ближайшему целому.
// Пример работы:
// round(6.2);
// => 6
// round(6.8);
// => 7
// round(6.5);
// => 7

var round = function(number) {
    var result = 0;
    var remainer = number % 1;
    if (remainer >= 0.5) {
        result = 1 + number - remainer;
    } else {
        result = number - remainer;
    } return result;
};

// Создать функцию random с двумя (или одним) входными параметрами (min, max).
// Возвращает случайное целое число (можно использовать Math.random и Math.round), в диапазоне от min до max, включительно.
// Если вы передали только один агрумент, будет использован диапазон от 0 до переданного числа (т.е. до max).
// Пример работы:
// random(0, 100);
// => 42

var random = function(min, max) {
    if (max) {
        return Math.round(Math.random() * (max - min)) + min;
    } else {
        max = min;
        min = 0;
        return Math.round(Math.random() * max) + min;
    }
};

// Создать функцию keys которая возвращает массив имен всех свойств(ключей) принимаемого объекта.
// Пример работы:
// keys({one: 1, two: 2, three: 3});
// => ["one", "two", "three"]

var keys = function(object) {
    var keys = [];
    var prop;
    for (prop in object) {
        keys[keys.length] = prop;
    }
    return keys;
};

// Создать функцию values которая возвращает массив значений всех свойств принимаемого объекта.
// Пример работы:
// values({one: 1, two: 2, three: 3});
// => [1, 2, 3]

var values = function(object) {
    var values = [];
    var prop;
    for (prop in object) {
      if (object.hasOwnProperty(prop)) {
          values[values.length] = object[prop];
      }
    }
    return values;
};

// Создать функцию pairs которая возвращает список пар [свойство, значение] входящего объекта.
// Пример работы:
// pairs({one: 1, two: 2, three: 3});
// => [["one", 1], ["two", 2], ["three", 3]]

var pairs = function(object) {
    var pairs = [];
    var prop;
    for (prop in object) {
        if(object.hasOwnProperty(prop)) {
            pairs[pairs.length] = [prop, object[prop]];
        }
    }
    return pairs;
};

// Создать функцию invert которая возвращает копию входящего объекта где свойства - значения, а значения - свойства.
// Чтобы это заработало, нужно, чтобы все значения свойств объекта могли быть уникально преобразованы в строки.
// Пример работы:
// invert({Moe: "Moses", Larry: "Louis", Curly: "Jerome"});
// => {Moses: "Moe", Louis: "Larry", Jerome: "Curly"}

var invert = function(object) {
    var newObject = {};
    var prop;
    for (prop in object) {
        newObject[object[prop]] = prop;
    }
    return newObject;
};

// Создать функцию omit которая возвращает копию объекта без указанного свойства.
// Пример работы:
// omit({name: 'moe', age: 50, userid: 'moe1'}, 'userid');
// => {name: 'moe', age: 50}

var omit = function(object, omitValue) {
  var prop;
  var newObject = {};
  for (prop in object) {
    if (object.hasOwnProperty(prop)) {
        if (prop !== omitValue) {
            newObject[prop] = object[prop];
        }
    }
  } return newObject;
};

// Создать функцию has которая проверяет, содержит ли объект указанный ключ (свойство). Если да, то возвращает true иначе false.
// Пример работы:
// has({a: 1, b: 2, c: 3}, 'b');
// => true

var has = function(object, searchProp) {
  var prop;
  for (prop in object) {
      if (prop === searchProp) {
          return true;
      }
  } return false;
};

// Создать функцию isMatch которая проверяет, содержатся ли ключи-значения в объекте. Если да, то возвращает true иначе false.
// Пример работы:
// isMatch({name: 'moe', age: 32}, {age: 32});
// => true

var isMatch = function(object, object2) {
  var prop;
  for (prop in object) {
      if (object.hasOwnProperty(prop)) {
          for (prop in object2) {
              if (object2.hasOwnProperty(prop)) {
                  if (prop === prop && object[prop] === object2[prop]) {
                      return true;
                  }
              }
          }
      }
  } return false;
};

// Создать функцию isEmpty которая вернёт true если коллекция (объект или массив) не содержит ни одного значения, в противном случае вернет false.
// Пример работы:
// isEmpty([]);
// => true
// isEmpty([1, 2, 3]);
// => false
// isEmpty({});
// => true
// isEmpty({x: 4});
// => false

var isEmpty = function(object) {
    var prop;
    for(prop in object) {
        if(object.hasOwnProperty(prop))
            return false;
    }
    return true;
};

// Создать функцию extend с двумя входными параметрами (объект destination и объект source).
// Скопирует все свойства из объекта source в объект destination.
// Если объект source имеет одноименные свойства с объектом destination, то значения destination будут затёрты значениями из source.
// Пример работы:
// extend({name: 'moe'}, {age: 50});
// => {name: 'moe', age: 50}


var extend = function(destination, source) {
    var prop;
    var prop2;

    for (prop in source) {
        prop2 = prop;
        if (source.hasOwnProperty(prop)) {
            destination[prop2] = source[prop];
        }
    }

    return destination;
};

/* var objectSource = {
    name: 'John',
    age: 22,
    lovesPozniaky: true,
    isTom: true,
    month: 'jan'
};

var objectDestination = {
    name: "Tom",
    age: 33,
    test: 'test',
    country: 'Denmark',
    month: 'feb'
}; */

// Создать функцию defaults с двумя входными параметрами (объект object и объект default).
// Функция defaults проинициализирует неопределённые (undefined) свойства объета значениями одноимённых свойств из default.
// Если же какие-то свойства объекта уже определены, то они не будут изменены.
// Пример работы:
// defaults({flavor: "chocolate"}, {flavor: "vanilla", sprinkles: "lots"});
// => {flavor: "chocolate", sprinkles: "lots"}

var defaults = function(object, defaults) {
    var prop;
    for (prop in defaults) {
        if (defaults.hasOwnProperty(prop)) {
            if (object[prop] === undefined) {
                object[prop] = defaults[prop];
            }
        }
    } return object;
};

// Создать функцию each с двумя входными параметрами (массив и функция iteratee).
// Функция each проходит по всему списку элементов, вызывая для каждого из них функцию iteratee.
// При каждом вызове в iteratee будут переданы два аргумента: (element, index).
// Пример работы:
// each([1, 2, 3], function(element, index) { console.log(element, index); });
// => выведет в консоль все цифры и соответствующие им индексы по очереди

var each = function(array, iteratee) {
    var i;
    for (i = 0; i < array.length; i++) {
        iteratee(array[i], i);
    }
};

// Создать функцию map с двумя входными параметрами (массив и функция iteratee).
// Функция map возвращает новый массив, полученный преобразованием каждого элемента массива в функции iteratee.
// Функция iteratee получает два аргумента: значение value, индекс index.
// Пример работы:
// map([1, 2, 3], function(value) { return value * 3; });
// => [3, 6, 9]

var map = function(array, iteratee) {
    var newArray = [];
    var i;

    for (i = 0; i < array.length; i++) {
        newArray[newArray.length] = iteratee(array[i], i);
    }
    return newArray;
};

// Создать функцию findIndex с двумя входными параметрами (массив и функция predicate).
// Функция findIndex так же как и indexOf, возвращает первый индекс того значения, для которого функция predicate вернёт true.
// Если такой элемент не был найден, вернёт -1.
// Пример работы:
// findIndex([4, 6, 8, 12], function(value) { return value === 8; });
// => 2

var findIndex = function(array, predicate) {
    var i;
    for (i = 0; i < array.length; i++) {
        if (predicate(array[i]) === true) {
            return i;
        }
    } return -1;
};

// Создать функцию find с двумя входными параметрами (массив list и функция predicate).
// Функция find вызывает для каждого элемента list функцию сравнения predicate,
// возвращая первый элемент, для которого predicate вернула true, или undefined, если ни один элемент не подошёл.
// Пример работы:
// find([1, 2, 3, 4, 5, 6], function(num) { return num % 2 == 0; });
// => 2

var find = function(list, predicate) {
    var i;
    for (i = 0; i < list.length; i++) {
        if (predicate(list[i]) === true) {
            return list[i];
        }
    } return undefined;
};


// Создать функцию filter с двумя входными параметрами (массив list и функция predicate) которая проходит через каждое значение list,
// возвращая массив всех значений, для которых predicate вернул true.
// Пример работы:
// filter([1, 2, 3, 4, 5, 6], function(num) { return num % 2 == 0; });
// => [2, 4, 6]

var filter = function(list, predicate) {
    var i;
    var filteredArray = [];
    for (i = 0; i < list.length; i++) {
        if (predicate(list[i]) === true) {
         filteredArray[filteredArray.length] = list[i];
        }
    } return filteredArray;
};

// Создать функцию reject с двумя входными параметрами (массив list и функция predicate) которая возвращает массив,
// содержащий все значения list, за исключением элементов, для которых функция predicate вернула значение true. Т.е. reject является «антонимом» filter.
// Пример работы:
// reject([1, 2, 3, 4, 5, 6], function(num){ return num % 2 == 0; });
// => [1, 3, 5]

var reject = function(list, predicate) {
    var i;
    var filteredArray = [];
    for (i = 0; i < list.length; i++) {
        if (predicate(list[i]) === false) {
            filteredArray[filteredArray.length] = list[i];
        }
    } return filteredArray;
};


// Создать функцию every с двумя входными параметрами (массив list и функция predicate). Вернёт true, если для каждого значения из list predicate вернёт true.
// Пример работы:
// every([2, 4, 5], function(num) { return num % 2 == 0; });
// => false

var every = function(list, predicate) {
    var i;
    var count = 0;
    for (i = 0; i < list.length; i++) {
        if (predicate(list[i]) === true) {
            count++;
        }
    } return count === list.length;
};


// Создать функцию some с двумя входными параметрами (массив list и функция predicate). Вернёт true, если хотя бы для одного значения из list predicate вернёт true.
// Пример работы:
// some([2, 4, 5], function(num) { return num % 2 == 0; });
// => true

var some = function(list, predicate) {
    var i;
    for (i = 0; i < list.length; i++) {
        if (predicate(list[i]) === true) {
            return true;
        }
    }
    return false;
};

// Создать функцию partition с двумя входными параметрами (массив array и функция predicate).
// Разобъёт массив array на две части: одна - для элементов которой функция predicate вернёт true, и другая - для всех остальных.
// Пример работы:
// partition([0, 1, 2, 3, 4, 5], function(num) { return num % 2 == 0; });
// => [[0, 2, 4], [1, 3, 5]]

var partition = function(array, predicate) {
    var i;
    var trueItems = [];
    var falseItems = [];
    var newArray = [];
    for (i = 0; i < array.length; i++) {
        if (predicate(array[i]) === true) {
            trueItems[trueItems.length] = array[i];
        } else {
            falseItems[falseItems.length] = array[i];
        }
    }
    newArray = [trueItems, falseItems];
    return newArray;
};

// Создать функцию isCVV которая принимает строку. Возвращает true если строка состоит из 3 или 4 цифр. В реализации функции использовать регулярные выражения.
// Пример работы:
// isCVV('670');
// => true
// isCVV('0965');
// => true
// isCVV('69');
// => false

var isCVV = function(string) {
    var numbers = /^[0-9]{3,4}$/;
    return Boolean(string.match(numbers));
};

// Создать функцию isOnlyLettersOrNumbers которая принимает строку. Возвращает true если строка состоит только из букв английского алфавита или только из цифр.
// В реализации функции использовать регулярные выражения.
// Пример работы:
// isOnlyLettersOrNumbers('670');
// => true
// isOnlyLettersOrNumbers('tree');
// => true
// isOnlyLettersOrNumbers('b2b');
// => false

var isOnlyLettersOrNumbers = function(string) {
    var onlyLettersOrNumbers = /^[0-9]+$|^[a-zA-Z]+$/;
    return !!(string.match(onlyLettersOrNumbers));
};

// Создать функцию isLettersAndNumbers которая принимает строку. Возвращает true если строка состоит из смеси букв английского алфавита и цифр.
// В реализации функции использовать регулярные выражения.
// Пример работы:
// isLettersAndNumbers('670');
// => false
// isLettersAndNumbers('tree');
// => false
// isLettersAndNumbers('b2b');
// => true

var isLettersAndNumbers = function(string) {
    return !!(string.match(/[0-9]/) && string.match(/[A-Z]/i));
};

// Создать функцию isOnlyLettersAndNumbers которая принимает строку. Возвращает true если строка состоит из букв английского алфавита и цифр
// (наличие каждого типа символов необязательно). В реализации функции использовать регулярные выражения.
// Пример работы:
// isOnlyLettersAndNumbers('670');
// => true
// isOnlyLettersAndNumbers('tree');
// => true
// isOnlyLettersAndNumbers('b2b');
// => true
// isOnlyLettersAndNumbers('2+2=4');
// => false

var isOnlyLettersAndNumbers = function(string) {
    return !(string.match(/\W/));
};

// Создать функцию-конструктор Circle которая принимает 3 параметра: координаты центра окружности (x, y) и ее радиус (radius).
// Возвращает объект с собственными тремя свойствами (x, y, radius) и унаследованными тремя методами.
// 1. Метод getDiameter возвращает диаметр откружности. Формула расчета диаметра: diameter = 2 * radius
// 2. Метод getPerimeter возвращает длину откружности. Формула расчета длины окружности: perimeter = Math.PI * diameter
// 3. Метод getSquare возвращает площадь откружности. Формула расчета площади окружности: square = Math.PI * (radius в квадрате)
// Пример работы:
// var circle = new Circle(5, 5, 5);
// circle.getDiameter();
// => 10
// circle.getPerimeter();
// => 31.41592653589793
// circle.getSquare();
// => 78.53981633974483


var Circle = function(x, y, radius) {
    this.x = x;
    this.y = y;
    this.radius = radius;
};

Circle.prototype.getDiameter = function() {
    return this.radius * 2;
};

Circle.prototype.getPerimeter  = function() {
    return Math.PI * this.getDiameter();
};

Circle.prototype.getSquare = function() {
    return Math.PI * this.radius * this.radius;
};