// ВНИМАНИЕ! Соблюдайте форматирование кода (отступы, переносы)

// Объявить три переменных с произвольным именем.

var firstVariable;
var secondVariable;
var thirdVariable;

// Объявить еще 6 переменных и присвоить им значения произвольных типов данных.

var number = 1;
var string = 'JavaScript';
var boolean = true;
var array = [2, 'str', false];
var object = {
    projectName: 'js',
    startDate: 23
};

var myFunction = function(a) {
    if (a > 0) {
        return a + ' ' + 'is a greater than 0';
    } else {
        return a + " " + 'is not greater than 0';
    }
};

// Создать массив из элементов, значения которых представлены всеми возможными типами данных в JavaScript.

var allDataTypes = [
    23,
    'January',
    false,
    undefined,
    null,
    ['February'],
    {month: 'January', year: 2019, AD: true},
    function () {
        console.log('Hello JS')
    }
];

// Создать объект из четырех свойств. Два свойства должны иметь значения простых типов данных. Два остальных свойства должны иметь значения составного (объектного) типа данных.

var user = {
    name: 'John',
    logged: true,
    friends: ['Alex', 'Tom'],
    activity: {
        updates: 22,
        visits: 12,
        registrationMonth: 'May'
    }
};

// Создать 5 объектов представляющих объекты реального мира. В каждом объекте должно быть не меньше 4 свойств.

var apartment = {
    location: 'Osokorky',
    area: 65,
    isSmartHome: true,
    residents: ['Tom', 'Jerry']
};

var coffee = {
    price: 30,
    isStrong: false,
    madeBy: 'Aroma Kava',
    wakeUp: function() {
        console.log('WAKE UP');
    }
};

var js = {
    isInterpreted: true,
    fullName: 'JavaScript',
    dataTypes: ['number', 'string', 'boolean', 'undefined', 'null', 'array', 'object', 'function'],
    libraries: ['React', 'jQuery']
};

var KPI = {
    fullName: 'National Technical University of Ukraine "Kyiv Politechnic Institute"',
    location: 'Politekhnichnyi Instytut metro station',
    numberOfStudents: 40500,
    events: [
        {
            name: 'International Conference',
            date: 'Jan, 30'
        },
        {
            name: 'Technical Conference',
            date: 'Feb, 20'
        },
        {
            name: 'Translation Conference',
            date: 'Feb, 21',
            location: 'Peremohy Ave, 37'
        }
    ]
};

var businessCard3 = {
    status: 'done',
    isWithSass: true,
    isWithJS: false,
    pagesNumber: 4
};

// Создать 5 массивов представляющих массивы некоторых значений из реальной жизни. В каждом массиве должно быть не меньше 4 элементов.

var coffeeTypes = ['espresso', 'americano', 'latte', 'cappuccino'];
var ingredients = ['coffee', 'milk', 'sugar', 'water'];
var shoppingList = [
    ["chewing gum", 1],
    ["tea", 1],
    ["cake", 2],
    ["apples", 4]
];
var meansOfTranspot = [
    {
        name: 'metro',
        travelTime: 35,
        isAvailable: true
    },
    {
        name: 'bus',
        travelTime: 45,
        isAvailable: true
    },
    {
        name: 'bicycle',
        travelTime: 50,
        isAvailable: false
    },
    {
        name: 'on foot',
        travelTine: 120,
        isAvailable: true
    }
];
var daysOff = [1, 5, 6, 7, 12, 13, 19, 20, 26, 27];

// Создать массив из трех элементов и написать выражение возвращающее значение второго элемента.

var months = ['March', 'April', 'May'];
months[1];
var showSecond = function (arr) {
    return arr[1];
};

// Создать объект из трех свойств и написать выражение возвращающее значение третьего свойства указанного при определении объекта.

var phone = {
    make: 'HTC',
    model: 'U11',
    color: 'red'
};

var showColor = function (obj) {
    return phone.color;
};

// Создать массив чисел (не менее 5 чисел) и вычислить сумму этих чисел возведенных в квадрат используя цикл while.

var numbers = [100, 200, 300, 400, 500, 600, 700];
var i = 0;
var sum = 0;
while (i < numbers.length) {
    sum += Math.pow(numbers[i], 2);
    i++;
}

// Создать функцию с двумя входными параметрами (числового типа данных) которая возвращает наибольшее из них.

var showGreaterNumber = function (a, b) {
    if (a > b) {
        return a;
    }

    return b;
};

// Создать функцию с двумя входными параметрами (числового типа данных) которая возвращает их сумму. Вызвать ее с произвольными аргументами (числового типа данных).

var getSum = function (a, b) {
    return a + b;
};
getSum(13, 13);

// Создать такие структуры данных чтобы выражение
// dro[1]().bro вернуло в качестве результата значение true,

var dro = ['placeholder',
    function () {
        return {
            bro: true,
            test: false
        }
    }
];

// выражение a[4][1][1].y вернуло строку 'Север',

var a = [
    'placeholder',
    'placeholder',
    'placeholder',
    'placeholder',
    [
        'placeholder2',
        [
            'placeholder3',
            {
                x: 'Юг',
                y: 'Север'
            }
        ]
    ]
];

// выражение b.y().y.z()[3].autor вернуло строку 'Дима'.

var b = {
    y: function () {
        return {
            y: {
                z: function () {
                    return ['placeholder', 'placeholder', 'placeholder', {autor: 'Дима'}];
                }
            }
        }
    }
};

// Создать объект со свойствами: x, getX, changeX. Где значение свойства "x" это число, а getX и changeX это методы которые манипулируют значением этого свойства "x". getX возвращает значение свойства "x", а changeX принимает в качестве аргумента число и результатом работы этого метода является присваивание этого числа свойству "x" объекта.

var dragon = {
    x: 23,
    getX: function () {
        return this.x;
    },
    changeX: function (newX) {
        this.x = newX;
    }
};