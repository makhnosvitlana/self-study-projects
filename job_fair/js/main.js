var showAnotherItem = function(itemsCollection, incrementor , paginationCollection) {
    var activeIndex = itemsCollection.filter('.active').index();
    if (activeIndex < itemsCollection.length) {
        if (activeIndex === itemsCollection.length-1) {
            activeIndex = -1;
        }
        itemsCollection.get(activeIndex).classList.remove('active');
        activeIndex += incrementor ;
        itemsCollection.get(activeIndex).classList.add('active');

        if (paginationCollection) {
            changePagination(itemsCollection, paginationCollection, activeIndex);
        }
    }
};

var changePagination = function (itemsCollection, paginationCollection, activeIndex) {
    paginationCollection.filter('.selected').removeClass('selected');
    paginationCollection.get(activeIndex).classList.add('selected');
    itemsCollection.filter('.active').removeClass('active');
    itemsCollection.get(activeIndex).classList.add('active');
};

var newsView = {
    subscribe: function() {
        var newsCollection = $('.news__item');

        $('.news__btn--next').on('click', function () {
            showAnotherItem(newsCollection, 1);
        });

        $('.news__btn--previous').on('click', function () {
            showAnotherItem(newsCollection, -1);
        })
    }
};

newsView.subscribe();

var bannerView = {
    subscribe: function () {
        var bannersCollection = $('.banner__item');
        var paginationCollection= $('.banner__pagination a');

        $('.banner__btn--next').on('click', function () {
            showAnotherItem(bannersCollection, 1, paginationCollection);
        });

        $('.banner__btn--previous').on('click', function () {
            showAnotherItem(bannersCollection, -1, paginationCollection);
        });

        paginationCollection.on('click', function () {
            var activeIndex = $(this).index();
            changePagination(bannersCollection, paginationCollection, activeIndex);
        });

        $('.toggle-banner-btn').on('click', function () {
            $('.banner').toggle('fast', 'linear');
            $(this).toggleClass('hidden');
        })
    }
};

bannerView.subscribe();


////////TEST with prototype
//
// var ClickableItemsGroup = function (itemsCollection, paginationCollection) {
//     this.itemsCollection = itemsCollection;
//     this.paginationCollection = paginationCollection;
// };
//
// ClickableItemsGroup.prototype.showAnotherItem = function(incrementor) {
//     var activeIndex = this.itemsCollection.filter('.active').index();
//     if (activeIndex < this.itemsCollection.length) {
//         if (activeIndex === this.itemsCollection.length-1) {
//             activeIndex = -1;
//         }
//         this.itemsCollection.get(activeIndex).classList.remove('active');
//         activeIndex += incrementor ;
//         this.itemsCollection.get(activeIndex).classList.add('active');
//
//         if (this.paginationCollection) {
//             this.changePagination(this.itemsCollection, this.paginationCollection, activeIndex);
//         }
//     }
// };
//
// ClickableItemsGroup.prototype.changePagination = function (itemsCollection, paginationCollection, activeIndex) {
//     paginationCollection.filter('.selected').removeClass('selected');
//     paginationCollection.get(activeIndex).classList.add('selected');
//     itemsCollection.filter('.active').removeClass('active');
//     itemsCollection.get(activeIndex).classList.add('active');
// };
//
// var newsViewPr = new ClickableItemsGroup($('.news__item'));
// var bannerViewPr = new ClickableItemsGroup($('.banner__item'), $('.banner__pagination a'));
//
//
// newsViewPr.subscribe = function () {
//     console.log(newsViewPr);
//
//     $('.news__btn--next').on('click', function () {
//         newsViewPr.showAnotherItem(1);
//     });
//
//     $('.news__btn--previous').on('click', function () {
//         newsViewPr.showAnotherItem(-1);
//     })
// };
//
// bannerViewPr.subscribe = function () {
//     $('.banner__btn--next').on('click', function () {
//         bannerViewPr.showAnotherItem(1, bannerViewPr.paginationCollection);
//     });
//
//     $('.banner__btn--previous').on('click', function () {
//         bannerViewPr.showAnotherItem(-1, bannerViewPr.paginationCollection);
//     });
//
//     this.paginationCollection.on('click', function () {
//         var activeIndex = $(this).index();
//         bannerViewPr.changePagination(bannerViewPr.itemsCollection, bannerViewPr.paginationCollection, activeIndex);
//         });
//
//     $('.toggle-banner-btn').on('click', function () {
//         $('.banner').toggle();
//         $(this).toggleClass('hidden');
//     })
// };
//
// newsViewPr.subscribe();
// bannerViewPr.subscribe();