var profilesCollection = {
    models: [],

    init: function() {
        // Fetch data from localStorage and save it to this.models
        this.models = JSON.parse(window.localStorage.getItem('profilesCollection')) || [];
    },

    add: function(profile) {
        // Add model to the collection
        // Synchronize the collection with localStorage
        this.models.push(profile);
        this.updateStorage();
    },

    getProfileById: function(profileId) {
        // Return a model from the collection by ID
        return _.findWhere(this.models, {id: profileId});
    },

    removeProfileById: function(profileId) {
        // Удалить модель из коллекции
        // Cинхронизировать коллекцию с localStorage
        this.models = _.reject(this.models, function(element) {
            return element.id === profileId;
        });
        this.updateStorage();
    },

    updateProfile: function(updatedProfile) {
        // Find the model in the collection and update it
        // Synchronize the collection with localStorage
        this.models.forEach(function (element) {
            if (element.id === updatedProfile.id) {
                element = _.extend(element, updatedProfile);
            }
        });
        this.updateStorage();
    },

    updateStorage: function() {
        // Synchronize the collection with localStorage
        window.localStorage.setItem('profilesCollection', JSON.stringify(this.models));
    }
};

profilesCollection.init();

var listView = {
    tmplFn: doT.template($('#profiles-template').html()),

    render: function() {
        // Render component elements
        // Subscribe to events
        $('.profiles-container').html(this.tmplFn(profilesCollection.models));
        this.subscribe();
    },

    subscribe: function() {
        // Subscribe to events:
        // 1. Click the profile item
        // 2. Click the Add btn
        $('.profiles-list__item').on('click', function () {
            var selectedProfileId = $(this).attr('id');
            editFormView.render(profilesCollection.getProfileById(selectedProfileId));
        });
        $('.btn--add-profile').on('click', function () {
            addFormView.render();
        })
    }
};

listView.render();

var addFormView = {
    tmplFn: doT.template($('#add-form-template').html()),

    render: function() {
        // Render component elements
        // Subscribe to events
        $('.form-container').html(this.tmplFn());
        this.subscribe();
    },

    subscribe: function() {
        // Subscribe to events:
        // 1. Click the Save btn
        $('.btn--save').on('click', function () {
            profilesCollection.add(addFormView.getFormData());
            addFormView.remove();
            listView.render();
        })
    },

    getFormData: function() {
        // Save form data as an object
        var addFormInputs = $('.add-profile-form input');
        var profile = {};
        addFormInputs.each(function (index, input) {
            profile[input.name] = input.value;
        });
        profile.id = addFormView.getUniqId();
        return profile;
    },

    getUniqId: function() {
        return '_' + Math.random().toString(36).substr(2, 9);
    },

    remove: function() {
        // Remove component from the screen
        $('.add-profile-form').remove();
    }
};

var editFormView = {
    tmplFn: doT.template($('#edit-form-template').html()),

    render: function(profile) {
        // Render component elements
        // Subscribe to events
        $('.form-container').html(editFormView.tmplFn(profile));
        this.subscribe();
    },

    subscribe: function() {
        // Subscribe to events:
        // 1. Click the Update btn
        // 2. Click the Delete btn
        $('.btn--update').on('click', function () {
            profilesCollection.updateProfile(editFormView.getFormData());
            listView.render();
            editFormView.remove();
        });

        $('.btn--delete').on('click', function () {
            profilesCollection.removeProfileById(editFormView.getFormData().id);
            editFormView.remove();
            listView.render();
        })
    },

    getFormData: function() {
        // Save form data into an object
        var addFormInputs = $('.edit-profile-form input');
        var updatedProfile = {};
        addFormInputs.each(function (index, input) {
            updatedProfile[input.name] = input.value;
        });
        updatedProfile.id = $('.edit-profile-form').attr('id');
        return updatedProfile;
    },

    remove: function() {
        // Remove component from screen
        $('.edit-profile-form').remove();
    }
};
