import React from 'react';

function ListItem(props) {
    function handleClick() {
        props.showUpdForm(props.profile);
    }
    return (
        <li onClick={handleClick} className="list__item">
            {props.profile.nickname} {props.profile.age} {props.profile.price}
        </li>
    );
}

export default ListItem;
