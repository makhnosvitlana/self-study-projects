import React from 'react';
import ListItem from './ListItem';

function List(props) {
    const profiles = props.data.map(item => (
        <ListItem key={item.id} profile={item} showUpdForm={props.showUpdForm} />
    ));
    return <ul className="list">{profiles}</ul>;
}

export default List;
