import React, { Component } from 'react';

class AddForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            firstname: '',
            lastname: '',
            nickname: '',
            age: '',
            price: '',
            id:
                "_" +
                Math.random()
                    .toString(36)
                    .substr(2, 9)
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        const { name, value } = event.target;
        this.setState({
            [name]: value
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.addProfile(this.state);
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit} action="" className="form add-form">
                <legend>Fill out these fields to register</legend>
                <div className="form__field">
                    <label htmlFor="name">First Name</label>
                    <input
                        type="text"
                        id="firstname"
                        name="firstname"
                        value={this.state.firstname}
                        onChange={this.handleChange}
                    />
                </div>
                <div className="form__field">
                    <label htmlFor="age">Last Name</label>
                    <input
                        type="text"
                        id="lastname"
                        name="lastname"
                        value={this.state.lastname}
                        onChange={this.handleChange}
                    />
                </div>
                <div className="form__field">
                    <label htmlFor="age">Nickname</label>
                    <input
                        type="text"
                        id="nickname"
                        name="nickname"
                        value={this.state.nickname}
                        onChange={this.handleChange}
                    />
                </div>
                <div className="form__field">
                    <label htmlFor="age">Age</label>
                    <input
                        type="text"
                        id="age"
                        name="age"
                        value={this.state.age}
                        onChange={this.handleChange}
                    />
                </div>
                <div className="form__field">
                    <label htmlFor="age">Price</label>
                    <input
                        type="text"
                        id="price"
                        name="price"
                        value={this.state.price}
                        onChange={this.handleChange}
                    />
                </div>
                <button className="btn btn--upd">Save</button>
            </form>
        );
    }
}

export default AddForm;
