import React, { Component } from 'react';

class UpdateForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            firstname: this.props.profile.firstname,
            lastname: this.props.profile.lastname,
            nickname: this.props.profile.nickname,
            age: this.props.profile.age,
            price: this.props.profile.price,
            id: this.props.profile.id
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.profile.id !== prevState.id) {
            return {
                name: nextProps.profile.name,
                age: nextProps.profile.age,
                id: nextProps.profile.id
            };
        } else {
            return null;
        }
    }

    handleChange(event) {
        const { name, value } = event.target;
        this.setState({
            [name]: value
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.updateProfile(this.state);
    }

    handleDelete(event) {
        event.preventDefault();
        this.props.deleteProfile(this.state.id);
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit} action="" className="form upd-form">
                <legend>Update or Delete Your Profile</legend>
                <div className="form__field">
                    <label htmlFor="name">First Name</label>
                    <input
                        type="text"
                        id="firstname"
                        name="firstname"
                        value={this.state.firstname}
                        onChange={this.handleChange}
                    />
                </div>
                <div className="form__field">
                    <label htmlFor="age">Last Name</label>
                    <input
                        type="text"
                        id="lastname"
                        name="lastname"
                        value={this.state.lastname}
                        onChange={this.handleChange}
                    />
                </div>
                <div className="form__field">
                    <label htmlFor="age">Nickname</label>
                    <input
                        type="text"
                        id="nickname"
                        name="nickname"
                        value={this.state.nickname}
                        onChange={this.handleChange}
                    />
                </div>
                <div className="form__field">
                    <label htmlFor="age">Age</label>
                    <input
                        type="text"
                        id="age"
                        name="age"
                        value={this.state.age}
                        onChange={this.handleChange}
                    />
                </div>
                <div className="form__field">
                    <label htmlFor="age">Price</label>
                    <input
                        type="text"
                        id="price"
                        name="price"
                        value={this.state.price}
                        onChange={this.handleChange}
                    />
                </div>
                <button className="btn btn--upd">Update</button>
                <button className="btn btn--delete" onClick={this.handleDelete}>Delete</button>
            </form>
        );
    }
}

export default UpdateForm;
