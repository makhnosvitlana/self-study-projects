import React, { Component } from 'react';
import AddForm from './AddForm';
import UpdateForm from './UpdateForm';
import List from './List';
import '../css/style.css';
import _ from 'underscore';

let profiles = JSON.parse( window.localStorage.getItem('profiles')) || [];

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: profiles,
            showAddFrom: false,
            showUpdForm: false,
            showVip: true,
            selectedProfile: {}
        };
        this.handleAddClick = this.handleAddClick.bind(this);
        this.handleItemClick = this.handleItemClick.bind(this);
        this.addProfile = this.addProfile.bind(this);
        this.updateProfile = this.updateProfile.bind(this);
        this.deleteProfile = this.deleteProfile.bind(this);
        this.handleVip = this.handleVip.bind(this);
    }

    handleAddClick() {
        this.setState({
            showAddFrom: true,
            showUpdForm: false
        });
    }

    handleItemClick(profile) {
        this.setState({
            showUpdForm: true,
            showAddFrom: false,
            selectedProfile: profile
        });
    }

    addProfile(newData) {
        profiles.push(newData);
        this.setState({
            showAddFrom: false
        });
    }

    updateProfile(selected) {
        const selectedProfile = _.findWhere(profiles, { id: selected.id });
        _.extend(selectedProfile, selected);
        this.setState({
            showUpdForm: false
        });
    }

    deleteProfile(selectedId) {
        profiles = _.reject(profiles, item => item.id === selectedId);
        this.setState({
            data: profiles,
            showUpdForm: false
        });
    }

    handleVip(event) {
        let profilesVip;
        if (event.target.checked) {
            profilesVip = _.filter(this.state.data, (obj) => {
                return obj.price > 100;
            });
        } else {
            profilesVip = profiles;
        }
        this.setState({
            data: profilesVip
        })
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        window.localStorage.setItem('profiles', JSON.stringify(profiles));
    };

    render() {
        return (
            <div className="app">
                <div className="list-container">
                    <button onClick={this.handleAddClick} className="btn btn--add">Add Profile</button>
                    <div className="vip">
                        <label htmlFor="vip">VIP</label>
                        <input type="checkbox" onChange={this.handleVip} id="vip"/>
                    </div>
                    {this.state.data.length
                        ? <List data={this.state.data} showUpdForm={this.handleItemClick} />
                        : <p className="no-profiles">No profiles yet</p>
                    }
                </div>
                {this.state.showAddFrom &&
                    <AddForm addProfile={this.addProfile} />}
                {this.state.showUpdForm &&
                    <UpdateForm
                        profile={this.state.selectedProfile}
                        updateProfile={this.updateProfile}
                        deleteProfile={this.deleteProfile}
                    />
                }
            </div>
        );
    }
}

export default App;
